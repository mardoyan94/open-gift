import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Keyboard,
    Image,
    Modal,
    Alert
} from 'react-native';
import API from '../../networking/api'
import { Rating, AirbnbRating } from 'react-native-ratings';
import { GoogleApi } from '../../networking/google.api'

export default class OfferModal extends Component {

    itemSym = ['A', 'B', 'C']

    api = new API()

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    deleteDriverOffers() {
        let bodyData = {
            driver: this.props.offer.driver
        }
        this.api.deleteAllOffers(bodyData).then((res) => {
            console.log(res);
            this.props.goBack()
        })
            .then((error) => {
                console.log(error);

            })
    }

    acceptDrivingForDriver() {
        let bodyData = {
            cost: this.props.offer.amount,
            status: 'driver_found',
            driver: this.props.offer.driver
        }
        this.api.drivings('PATCH', bodyData, 0, this.props.trip._id, this.props.trip._etag)
            .then((res) => {
                console.log(res);
                this.deleteDriverOffers()
            })
            .catch((error) => {
                console.log(error);
            })
    }

    cancel() {
        const { offer } = this.props

        this.api.deleteOffer(offer._id, offer._etag).then((res) => {
            console.log(res);
            console.log('createDrivingsOffer!!!!!!!!!!!!!!!!!!!!!');

            this.props.goBack()
        })
            .catch((error) => {
                console.log(error);

            })
    }

    changePrice() {
        console.log('+++++++++++++++++++++++++++++++++++++++++');

        console.log(this.props.trip);
        let costObj = {}
        let locationObj = {}
        let bodyData = {}
        if (this.props.offer.amount) {
            costObj = {
                cost: this.props.offer.amount
            }
        }
        if (this.props.offer.location_to_lat) {
            let requestBody = {
                location_from_lat: this.props.trip.location_from_lat,
                location_from_lng: this.props.trip.location_from_lng,
                location_to_lat: this.props.offer.location_to_lat,
                location_to_lng: this.props.offer.location_to_lng
            }
            GoogleApi.getDistance(requestBody).then((info) => {
                console.log('info');
                console.log(info);

                let distance = info.rows[0].elements[0].distance.value
                let duration = info.rows[0].elements[0].duration.value

                locationObj = {
                    distance: distance,
                    duration: duration,
                    address_to: this.props.offer.address_to,
                    location_to_lat: this.props.offer.location_to_lat,
                    location_to_lng: this.props.offer.location_to_lng
                }
                bodyData = {
                    ...costObj,
                    ...locationObj
                }
                this.changeTrip(bodyData)
            })
                .catch((error) => {
                    console.log(error);

                })
        }
        else {
            bodyData = {
                ...costObj,
                ...locationObj
            }
            this.changeTrip(bodyData)
        }

    }

    changeTrip(bodyData) {
        this.api.drivings('PATCH', bodyData, 0, this.props.trip._id, this.props.trip._etag)
            .then((res) => {
                console.log(res);
                console.log('PATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCH');
                this.cancel()
            })
            .catch((error) => {
                console.log(error);
            })
    }


    _renderLocation() {
        const { offer } = this.props
        return offer.address_to ? (<View style={[styles.itemRow, { marginTop: 20 }]}>
            <Image
                style={{
                    width: 27,
                    height: 40.8
                }}
                source={require('../../assets/icons/marker.png')}
            />
            <View style={{ marginLeft: 10 }}>
                <Text style={styles.itemText}>
                    New address:
            </Text>
                <Text style={[styles.itemText, { color: '#2ecc71' }]}>
                    {offer.address_to}
                </Text>
                <View style={styles.oldInfoContainer}>
                    <Text style={styles.oldInfoText}>
                        {this.props.trip.address_to}
                    </Text>
                    <View style={styles.oldLine} />
                </View>
            </View>
        </View>) : null
    }

    _renderDestinations() {
        let destinations = JSON.parse(this.props.trip.destinations)
        return destinations.map((item, i) => (<View
            key={i}
            style={[
                styles.inputContainer,
                item.address == '' ?
                    { borderBottomColor: 'rgb(189, 195, 199)' } :
                    null]}>
            <View style={styles.pointIcon}>
                <Text style={styles.pointIconText}>
                    {this.itemSym[i]}
                </Text>
            </View>
            <Text
                style={[styles.input,
                { textAlignVertical: "center" },
                item.address == '' ? { color: 'rgb(189, 195, 199)' } : null]}
            >{item.address == '' ? 'New Location' : item.address}</Text>
           
        </View>))
    }

    _renderPrice() {
        const { offer } = this.props
        return offer.amount ? (<View style={[styles.itemRow, { marginTop: 20 }]}>
            <Image
                style={{
                    width: 27,
                    height: 32
                }}
                source={require('../../assets/icons/money.png')}
            />
            <View style={{ marginLeft: 10 }}>
                <Text style={styles.itemText}>
                    New price:
            <Text style={{
                        color: '#2ecc71'
                    }}>₦ {offer.amount}</Text>
                    
            </Text>
                <View style={styles.oldInfoContainer}>
                    <Text style={styles.oldInfoText}>
                    ₦{this.props.trip.cost.toFixed(2)}
            </Text>
                    <View style={styles.oldLine} />
                </View>
            </View>
        </View>) : null
    }

    _renderUserInfo() {
        return (<View style={[styles.itemRow, {
            marginTop: 20,
            marginBottom: 16
        }]}>
            <View style={styles.photoContainer}>
                <Image
                    style={styles.image}
                    source={this.props.user.avatar ?
                        { uri: 'data:image/png;base64,' + this.props.user.avatar } :
                        require('../../assets/images/1.png')}
                />
            </View>
            <View style={{
                marginLeft: 15
            }}>
                <View style={{
                    width: 83
                }}>
                    <AirbnbRating
                        defaultRating={4}
                        size={11}
                        isDisabled={true}
                        showRating={false} />
                </View>
                <Text style={styles.itemText}>
                    {this.props.user.firstname + ' ' + this.props.user.lastname}
                </Text>
            </View>
        </View>)
    }

    render() {
        const { offer } = this.props
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={() => { }}>
                <View
                    style={styles.container}>
                    <View style={styles.header}>
                        <Text style={styles.title}>
                            Changing Drive Offer
                </Text>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.goBack()
                            }}
                        >
                            <Image
                                style={{
                                    width: 20,
                                    height: 20
                                }}
                                source={require('../../assets/icons/close.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginHorizontal: 20 }}>
                        {this.props.user == null ? null : this._renderUserInfo()}
                        <View style={styles.silverLiner} />
                        {this._renderPrice()}
                        {this._renderLocation()}
                        {this._renderDestinations()}
                        <View style={[styles.silverLiner, { marginTop: 20 }]} />
                        <View style={[styles.itemRow, { justifyContent: "space-between" }]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.cancel()
                                }}
                                activeOpacity={0.8}
                                style={[styles.itemButton, { backgroundColor: '#fff' }]}
                            >
                                <Text style={[styles.buttonText, { color: '#d80027' }]}>
                                Decline offer
                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    if (this.props.trip.driver) {
                                        this.changePrice()
                                    }
                                    else {
                                        console.log('aaaaaaaaaaaaaaaaaaaaaaaaa');
                                        this.acceptDrivingForDriver()

                                    }

                                }}
                                activeOpacity={0.8}
                                style={styles.itemButton}
                            >
                                <Text style={[styles.buttonText, { color: '#fff' }]}>
                                Accept offer
                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        height: 50,
        paddingHorizontal: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#ecf0f1',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    title: {
        fontFamily: 'Roboto-Regular',
        fontSize: 20,
        color: 'rgb(44, 62, 80)'
    },
    itemRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemButton: {
        marginTop: 15,
        alignSelf: 'center',
        height: 40,
        width: '35%',
        backgroundColor: '#2ecc71',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    itemRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    photoContainer: {
        height: 48,
        width: 48,
        borderRadius: 24,
        backgroundColor: 'silver'
    },
    itemText: {
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)',
    },
    silverLiner: {
        height: 1,
        backgroundColor: '#e5e3dd',
    },
    oldInfoContainer: {
        alignSelf: 'flex-start',
        justifyContent: 'center',
    },
    oldInfoText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: 'rgb(189, 195, 199)',
    },
    oldLine: {
        position: 'absolute',
        height: 1,
        left: 0,
        right: 0,
        backgroundColor: 'rgb(189, 195, 199)'
    },
    image: {
        flex: 1,
        width: '100%',
        borderRadius: 28
    },
    inputContainer: {
        marginTop: 10,
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomColor: '#2ecc71',
        borderBottomWidth: 2
    },
    input: {
        flex: 1,
        height: 48,
        marginLeft: 5
    },
    pointIconText: {
        fontSize: 14,
        color: '#fff',
        fontFamily: 'Roboto-Regular'
    },
    pointIcon: {
        height: 22,
        width: 22,
        borderRadius: 10,
        backgroundColor: '#2ecc71',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
