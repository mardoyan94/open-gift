import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Modal,
    TextInput,
    TouchableWithoutFeedback,
    Keyboard,
    Alert
} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import API from '../../networking/api'

export default class ChangeOfferModal extends Component {

    itemSym = ['B', 'C', 'D']

    api = new API()
    constructor(props) {
        super(props)
        this.state = {
            newPrice: '',
        }
    }


    changeOrder() {
        if (this.props.price == '') {
            Alert.alert(
                '',
                'Select price!',
                [
                    { text: 'Close', onPress: () => { }, style: 'cancel' },
                ],
                { cancelable: false }
            )
        }
        else {
            let location = {}
            if (this.props.location.address != '') {
                location = {
                    address_to: this.props.location.address,
                    location_to_lat: this.props.location.location_to_lat,
                    location_to_lng: this.props.location.location_to_lng
                }
            }
            let amount = {}
            if (this.props.price != '') {
                amount = {
                    amount: Number(this.props.price),
                }
            }
            let bodyData = {
                driving: this.props.trip._id,
                confirmed_by_customer: true,
                confirmed_by_driver: false,

                ...amount
            }
            console.log('body________________________________________');

            console.log(this.props.price);

            console.log(bodyData);
            let setDest = this.props.destinations.filter(item => item.address != '').map((item) => {
                return {
                    lat: item.latitude,
                    lng: item.longitude,
                    address: item.address
                }
            })
            let changeDrivingBodyData = {
                destinations: JSON.stringify(setDest)
            }
            this.api.drivings('PATCH', changeDrivingBodyData, 0, this.props.trip._id, this.props.trip._etag).then((res) => {
                console.log('setDestinations');
                console.log(res);
                if (res._status == 'OK') {
                    this.api.createDrivingsOffer(bodyData).then((res1) => {
                        console.log(res1);
                        console.log('OKKKKKKK');

                        this.props.close(true)
                    })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert(
                                '',
                                'Server error!',
                                [
                                    { text: 'Close', onPress: () => { }, style: 'cancel' },
                                ],
                                { cancelable: false }
                            )
                        })
                }
                else {
                    Alert.alert(
                        '',
                        'Server error!',
                        [
                            { text: 'Close', onPress: () => { }, style: 'cancel' },
                        ],
                        { cancelable: false }
                    )
                }
                // this.props.goBack()
            })
                .catch((error) => {
                    console.log(error);
                    Alert.alert(
                        '',
                        'Server error!',
                        [
                            { text: 'Close', onPress: () => { }, style: 'cancel' },
                        ],
                        { cancelable: false }
                    )
                })

        }
    }

    _renderDriverInfo() {
        return this.props.user == null ?
            null :
            (<View><View style={[styles.itemRow, {
                marginTop: 20,
                marginBottom: 16
            }]}>
                <View style={styles.photoContainer}>
                    <Image
                        style={styles.image}
                        source={this.props.user.avatar ?
                            { uri: 'data:image/png;base64,' + this.props.user.avatar } :
                            require('../../assets/images/1.png')}
                    />
                </View>
                <View style={{
                    marginLeft: 15
                }}>
                    <View style={{
                        width: 83
                    }}>
                        <AirbnbRating
                            defaultRating={4}
                            size={11}
                            isDisabled={true}
                            showRating={false} />
                    </View>
                    <Text style={styles.userName}>
                        {this.props.user.firstname + ' ' + this.props.user.lastname}
                    </Text>
                </View>
            </View>
                <View style={styles.silverLiner} /></View>)
    }

    _renderAddButton(i) {
        if (this.props.destinations.length < 3 && i == 0) {
            return (<TouchableOpacity
                onPress={() => {
                    this.props.addPoint()
                }}
                activeOpacity={0.8}
                style={styles.toItemIconContainer}>
                <Image
                    source={require('../../assets/icons/add.png')}
                    style={styles.icon} />
            </TouchableOpacity>)
        }
        else {
            return (<TouchableOpacity
                onPress={() => {
                    this.props.deletePoint(i)
                }}
            >
                <Image
                    style={{
                        width: 20,
                        height: 20
                    }}
                    source={require('../../assets/icons/close.png')}
                />
            </TouchableOpacity>)

        }
    }

    _renderDestinations() {
        return this.props.destinations.map((item, i) => (<TouchableOpacity
            key={i}
            onPress={() => {
                this.props.openMap(i)
            }}
            style={[
                styles.inputContainer,
                item.address == '' ?
                    { borderBottomColor: 'rgb(189, 195, 199)' } :
                    null]}>
            <View style={styles.pointIcon}>
                <Text style={styles.pointIconText}>
                    {this.itemSym[i]}
                </Text>
            </View>
            <Text
                style={[styles.input,
                { textAlignVertical: "center" },
                item.address == '' ? { color: 'rgb(189, 195, 199)' } : null]}
            >{item.address == '' ? 'New Location' : item.address}</Text>
            {this._renderAddButton(i)}
        </TouchableOpacity>))
    }

    render() {
        console.log('=====================================');

        console.log(this.props.user);
        return (<Modal
            animationType="fade"
            transparent={false}
            visible={this.props.visible}
            onRequestClose={() => {
                this.props.close()
            }}>
            <TouchableWithoutFeedback
                onPress={() => {
                    Keyboard.dismiss()
                }}
            >
                <View
                    style={styles.container}>
                    <View style={styles.header}>
                        <Text style={styles.title}>
                            Changing Drive Offer
                </Text>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.close()
                            }}
                        >
                            <Image
                                style={{
                                    width: 20,
                                    height: 20
                                }}
                                source={require('../../assets/icons/close.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginHorizontal: 20 }}>
                        {this._renderDriverInfo()}
                        <View style={[
                            styles.inputContainer,
                            this.props.price == '' ?
                                { borderBottomColor: 'rgb(189, 195, 199)' } :
                                null]}>
                            <Text style={[styles.priceText, { color: '#2ecc71' }]}>
                                {'₦ '}
                            </Text>
                            <TextInput
                                onChangeText={(text) => {
                                    if (!isNaN(text)) {
                                        this.props.changePrice(text)
                                    }
                                }}
                                keyboardType='numeric'
                                style={styles.input}
                                value={this.props.price}
                                underlineColorAndroid="transparent"
                                placeholderTextColor='rgb(189, 195, 199)'
                                placeholder={'New Price'}
                            />
                        </View>
                        {this._renderDestinations()}
                        <View style={[styles.silverLiner, { marginTop: 20 }]} />
                        <View style={[styles.itemRow, { justifyContent: "space-between" }]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.close()
                                }}
                                activeOpacity={0.8}
                                style={[styles.itemButton, { backgroundColor: '#fff' }]}
                            >
                                <Text style={[styles.buttonText, { color: '#d80027' }]}>
                                    Cancel
                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.changeOrder()
                                }}
                                activeOpacity={0.8}
                                style={styles.itemButton}
                            >
                                <Text style={[styles.buttonText, { color: '#fff' }]}>
                                    Change
                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        height: 50,
        paddingHorizontal: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#ecf0f1',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    title: {
        fontFamily: 'Roboto-Regular',
        fontSize: 20,
        color: 'rgb(44, 62, 80)'
    },
    itemRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    photoContainer: {
        height: 48,
        width: 48,
        borderRadius: 24,
        backgroundColor: 'silver'
    },
    userName: {
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    silverLiner: {
        height: 1,
        backgroundColor: '#e5e3dd',
    },
    inputContainer: {
        marginTop: 10,
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomColor: '#2ecc71',
        borderBottomWidth: 2
    },
    input: {
        flex: 1,
        height: 48,
        marginLeft: 5
    },
    inputIcon: {
        width: 20.3,
        height: 14.3
    },
    itemButton: {
        marginTop: 15,
        alignSelf: 'center',
        height: 40,
        width: '35%',
        backgroundColor: '#2ecc71',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    image: {
        flex: 1,
        width: '100%',
        borderRadius: 28
    },
    toItemIconContainer: {
        width: 20,
        height: 20
    },
    icon: {
        width: 20,
        height: 20
    },
    pointIconText: {
        fontSize: 14,
        color: '#fff',
        fontFamily: 'Roboto-Regular'
    },
    pointIcon: {
        height: 22,
        width: 22,
        borderRadius: 10,
        backgroundColor: '#2ecc71',
        justifyContent: 'center',
        alignItems: 'center',
    },
    priceText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 20,
    }
});
