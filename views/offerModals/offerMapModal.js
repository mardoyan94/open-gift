import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Modal,
    TouchableWithoutFeedback,
    Keyboard,
    Dimensions,
    TextInput,
    ScrollView
} from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import { GoogleApi } from '../../networking/google.api'

export default class OfferMapModal extends Component {
    scrollView = null

    constructor(props) {
        super(props)
        this.state = {
            itemIndex: null,
            places: [],
            address: this.props.trip.address_to,
            latitude: this.props.trip.location_to_lat,
            longitude: this.props.trip.location_to_lng,
            actionVisible: true
        }

    }

    componentDidMount() {
        this.setAddresses(this.state.address)
    }

    setAddresses(e) {
        let request =
        {
            location: (this.state.latitude, + "," + this.state.longitude),
            radius: 200000,
            name: e
        }

        GoogleApi.autoComplate(request).then(res => {
            if (this.scrollView != null) {
                this.scrollView.scrollTo({ x: 0, y: 0, animated: false })
            }

            let addresses = res.map(d => {
                return {
                    short_name: d.structured_formatting.main_text,
                    address: d.structured_formatting.secondary_text,
                    place_id: d.place_id
                }
            })
            this.setState({
                places: addresses,
                itemIndex: null,
            })
        })
    }

    _renderPlaceList() {
        return this.state.places.map((item, i) => (<TouchableOpacity
            onPress={() => {
                console.log(item);
                GoogleApi.getLocationFromID(item.place_id).then((res) => {
                    let region = {
                        latitude: res.geometry.location.lat,
                        longitude: res.geometry.location.lng,
                        // latitudeDelta: 0.06,
                        // longitudeDelta: 0.06,
                        // short_name: this.state.places[this.state.itemIndex].short_name,
                    }
                    console.log(region);
                    this.map.animateToRegion({
                        ...region,
                        latitudeDelta: 0.06,
                        longitudeDelta: 0.06,
                    }, 500)

                }).catch((error) => {
                    console.log(error)
                })
                Keyboard.dismiss()
                this.setState({
                    itemIndex: i
                })
            }}
            key={i}
            style={[styles.cityItem, this.state.itemIndex == i ?
                { backgroundColor: '#2ecc71' } :
                null
            ]}
        >
            <Text style={[styles.itemText1, this.state.itemIndex == i ?
                { color: '#fff' } :
                null
            ]}>
                {item.short_name}
            </Text>
            <Text style={[styles.itemText2, this.state.itemIndex == i ?
                { color: '#fff' } :
                null]}>
                {item.address}
            </Text>
        </TouchableOpacity>))
    }

    _renderInicialMarker() {
        let center = Dimensions.get('window').height / 2 - 46
        return <View style={{
            position: 'absolute',
            zIndex: 1,
            top: center,
            alignItems: 'center',
            alignSelf: 'center'
        }}>
            <Image
                source={require('../../assets/images/userMarker.png')}
                style={{
                    flex: 1,
                    width: 44,
                    height: 53.3,
                }}
            />

        </View>
    }

    _renderSearchItem() {
        return (<View
            style={[styles.itemsContainer, {
                bottom: 15
            }]}>
            <View
                onPress={() => {

                }}
                activeOpacity={0.5}
                style={styles.item}>
                <TextInput
                    onChangeText={(text) => {
                        this.setAddresses(text)
                    }}
                    defaultValue={this.state.address}
                    placeholder='Search...'
                    style={styles.input} />

            </View>
            <ScrollView
                ref={ref => this.scrollView = ref}
                keyboardShouldPersistTaps='always'
            >
                {this._renderPlaceList()}
            </ScrollView>
            <View style={styles.buttonsContainer}>
                <TouchableOpacity
                    onPress={() => {
                        Keyboard.dismiss()
                        this.props.close()
                    }}
                    style={styles.button}
                    activeOpacity={0.7}
                >
                    <Text style={styles.buttonText}>
                        Close
                        </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        Keyboard.dismiss()
                        if (this.state.itemIndex == null) {
                            console.log(this.state.latitude);
                            let data = {
                                latitude: this.state.latitude,
                                longitude: this.state.longitude,
                                address: this.state.address,
                            }
                            console.log(data);

                            this.props.done(data)
                        }
                        else {
                            GoogleApi.getLocationFromID(this.state.places[this.state.itemIndex].place_id).then((res) => {
                                let region = {
                                    lat: res.geometry.location.lat,
                                    lng: res.geometry.location.lng,
                                    address: res.formatted_address,
                                    // latitudeDelta: 0.06,
                                    // longitudeDelta: 0.06,
                                    // short_name: this.state.places[this.state.itemIndex].short_name,
                                }
                                console.log(region);

                                this.props.done(region)
                            }).catch((error) => {
                                console.log(error)
                            })
                        }
                    }}
                    style={styles.button}
                    activeOpacity={0.7}
                >
                    <Text style={styles.buttonText}>
                        Done
                        </Text>
                </TouchableOpacity>
            </View>
        </View>)
    }

    render() {
        console.log('=====================================');
        console.log(this.props.point);

        return (<Modal
            animationType="fade"
            transparent={false}
            visible={this.props.visible}
            onRequestClose={() => {
                this.props.close()
            }}>
            <TouchableWithoutFeedback
                onPress={() => {
                    Keyboard.dismiss()
                }}
            >
                <View
                    style={styles.container}>
                    <View style={styles.header}>
                        <Text style={styles.title}>
                            Changing Drive Offer
                </Text>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.close()
                            }}
                        >
                            <Image
                                style={{
                                    width: 20,
                                    height: 20
                                }}
                                source={require('../../assets/icons/close.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    <MapView
                        onRegionChange={() => {
                            if (this.state.actionVisible) {
                                this.setState({
                                    actionVisible: false
                                })
                            }
                        }}
                        onRegionChangeComplete={(e) => {
                            GoogleApi.getPlace({
                                latitude: e.latitude,
                                longitude: e.longitude
                            }).then(responseJson => {
                                console.log(responseJson);
                                let data = {
                                    latitude: e.latitude,
                                    longitude: e.longitude,
                                    // short_name: responseJson[0].address_components[1].short_name,
                                    address: responseJson[0].formatted_address,
                                    // latitudeDelta: 0.06,
                                    // longitudeDelta: 0.06,
                                    // city: responseJson[2].address_components[2].long_name
                                    actionVisible: true
                                }
                                this.setState(data)
                                this.setAddresses(data.address)

                            }).catch((error) => {
                                console.log(error);

                            })
                        }}
                        ref={ref => this.map = ref}
                        style={styles.map}
                        initialRegion={{
                            latitude: this.props.point.address != '' ? this.props.point.latitude : this.state.latitude,
                            longitude: this.props.point.address != '' ? this.props.point.longitude : this.state.longitude,
                            latitudeDelta: 0.06,
                            longitudeDelta: 0.06,
                        }}
                    >
                        {/* <Marker
                            coordinate={{
                                latitude: this.state.latitude,
                                longitude: this.state.longitude,
                            }}
                        /> */}
                    </MapView>
                    {this._renderInicialMarker()}
                    {this.state.actionVisible ? this._renderSearchItem() : null}
                </View>
            </TouchableWithoutFeedback>
        </Modal>);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        // flex: 1,
        // position: 'absolute',
        height: Dimensions.get('window').height
    },
    header: {
        backgroundColor: '#fff',
        zIndex: 3,
        height: 50,
        paddingHorizontal: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#ecf0f1',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    title: {
        fontFamily: 'Roboto-Regular',
        fontSize: 20,
        color: 'rgb(44, 62, 80)'
    },
    itemsContainer: {
        position: 'absolute',
        height: 260,
        paddingTop: 5,
        left: 15,
        right: 15,
        backgroundColor: '#fff',
        borderRadius: 4,
        elevation: 5,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 20,
        shadowOpacity: 1,
    },
    item: {
        height: 48,
        marginHorizontal: 20,
        borderBottomColor: '#2ecc71',
        borderBottomWidth: 2,
        justifyContent: 'center',
    },
    input: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    rideButton: {
        marginTop: 10,
        height: 40,
        marginHorizontal: '30%',
        backgroundColor: '#2ecc71',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rideButtonText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: '#fff',
    },
    cityItem: {
        height: 60,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        justifyContent: 'center',
        paddingHorizontal: 20
    },
    itemText1: {
        color: 'rgb(44, 62, 80)',
        fontSize: 16,
        fontFamily: 'Roboto-Regular'
    },
    itemText2: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.1)'
    },
    buttonsContainer: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    button: {
        height: 50,
        paddingHorizontal: 20,
        justifyContent: 'center',
    },
    buttonText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: '#2ecc71'
    }
});
