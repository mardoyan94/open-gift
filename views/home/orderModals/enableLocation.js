import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Animated,
    Keyboard,
    ScrollView,
    TextInput
} from 'react-native';
import { GoogleApi } from '../../../networking/google.api'
import { connect } from 'react-redux'
import LocationSwitch from 'react-native-location-switch';

class EnableLocationClass extends Component {

    constructor(props) {
        super(props)
        this.state = {
            itemIndex: null,
            places: [],
            address: ''
        }
    }

    componentDidMount() {
        this.setAddresses(this.state.address)
    }

    onEnableLocationPress() {
        LocationSwitch.enableLocationService(1000, false,
            () => {
                this.getLocation()
            },
            () => { },
        );
    }

    getLocation() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                let coordinates = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                }
                this.props.dispatch({
                    type: 'ENTER_LOCATION',
                    value: true
                })
                GoogleApi.getPlace({
                    latitude: e.latitude,
                    longitude: e.longitude
                }).then(responseJson => {
                    ////console.log(responseJson);

                    this.props.dispatch({
                        type: 'SET_FROM',
                        value: {
                            latitude: e.latitude,
                            longitude: e.longitude,
                            short_name: responseJson[0].address_components[1].short_name,
                            address: responseJson[0].formatted_address,
                            latitudeDelta: 0.06,
                            longitudeDelta: 0.06,
                            city: responseJson[2].address_components[2] ? responseJson[2].address_components[2].long_name : responseJson[2].address_components[0].long_name
                        }
                    })
                }).catch((error) => {
                    //console.log(error);

                })
                this.props.done(coordinates)
            },
            (error) => {
                this.getLocation()
                console.log(error);
            },
            { enableHighAccuracy: false }
        );
    }

    setAddresses(e) {
        let request =
        {
            location: (this.props.map.from.latitude + "," + this.props.map.from.longitude),
            radius: 200000,
            name: e
        }

        GoogleApi.autoComplate(request).then(res => {
            this.scrollView.scrollTo({ x: 0, y: 0, animated: false })
            let addresses = res.map(d => {
                return {
                    short_name: d.structured_formatting.main_text,
                    address: d.structured_formatting.secondary_text,
                    place_id: d.place_id
                }
            })
            this.setState({
                places: addresses,
                itemIndex: null,
            })
        })
    }

    _renderPlaceList() {
        return this.state.places.length > 0 ?
            this.state.places.map((item, i) => (<TouchableOpacity
                onPress={() => {
                    Keyboard.dismiss()
                    // this.setState({
                    //     itemIndex: i
                    // })
                    GoogleApi.getLocationFromID(this.state.places[i].place_id).then((res) => {
                        let region = {
                            latitude: res.geometry.location.lat,
                            longitude: res.geometry.location.lng,
                            address: res.formatted_address,
                            latitudeDelta: 0.06,
                            longitudeDelta: 0.06,
                            short_name: this.state.places[i].short_name,
                            city: res.vicinity
                        }
                        this.props.dispatch({
                            type: 'ENTER_LOCATION',
                            value: true
                        })
                        GoogleApi.getPlace({
                            latitude: e.latitude,
                            longitude: e.longitude
                        }).then(responseJson => {
                            ////console.log(responseJson);

                            this.props.dispatch({
                                type: 'SET_FROM',
                                value: { ...region, }
                            })
                        }).catch((error) => {
                            //console.log(error);

                        })
                        this.props.done(region)
                    }).catch((error) => {
                        console.log(error)
                    })
                }}
                key={i}
                style={[styles.cityItem, this.state.itemIndex == i ?
                    { backgroundColor: '#2ecc71' } :
                    null
                ]}
            >
                <Text style={[styles.itemText1, this.state.itemIndex == i ?
                    { color: '#fff' } :
                    null
                ]}>
                    {item.short_name}
                </Text>
                <Text style={[styles.itemText2, this.state.itemIndex == i ?
                    { color: '#fff' } :
                    null]}>
                    {item.address}
                </Text>
            </TouchableOpacity>)) :
            (<Text style={styles.desc}>
                Your pick up location is not visible on the map. Turn on your phone's location
                service, to enable the app find a point for you
            </Text>)
    }

    render() {
        return (<View
            style={styles.itemsContainer}>
            <View
                onPress={() => {

                }}
                activeOpacity={0.5}
                style={styles.item}>
                <TextInput
                    onChangeText={(text) => {
                        this.setAddresses(text)
                    }}
                    defaultValue={this.state.address}
                    placeholder='Enter pickup point'
                    style={styles.input} />

            </View>
            <ScrollView
                ref={ref => this.scrollView = ref}
                keyboardShouldPersistTaps='always'
            >
                {this._renderPlaceList()}
            </ScrollView>
            <View style={styles.buttonsContainer}>
                <TouchableOpacity
                    onPress={() => {
                        Keyboard.dismiss()
                        this.onEnableLocationPress()
                    }}
                    style={styles.button}
                    activeOpacity={0.7}
                >
                    <Text style={styles.buttonText}>
                        Turn on location services
                        </Text>
                </TouchableOpacity>
            </View>
        </View>);
    }
}

export const EnableLocation = connect(
    ({ map }) => ({ map })
)(EnableLocationClass)

const styles = StyleSheet.create({
    itemsContainer: {
        position: 'absolute',
        height: 260,
        paddingTop: 5,
        left: 15,
        right: 15,
        top: 70,
        backgroundColor: '#fff',
        borderRadius: 4,
        elevation: 5,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 20,
        shadowOpacity: 1,
        zIndex: 1
    },
    item: {
        height: 48,
        marginHorizontal: 20,
        borderBottomColor: '#2ecc71',
        borderBottomWidth: 2,
        justifyContent: 'center',
    },
    input: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    rideButton: {
        marginTop: 10,
        height: 40,
        marginHorizontal: '30%',
        backgroundColor: '#2ecc71',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rideButtonText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: '#fff',
    },
    cityItem: {
        height: 60,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        justifyContent: 'center',
        paddingHorizontal: 20
    },
    itemText1: {
        color: 'rgb(44, 62, 80)',
        fontSize: 16,
        fontFamily: 'Roboto-Regular'
    },
    itemText2: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.1)'
    },
    buttonsContainer: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    button: {
        height: 50,
        paddingHorizontal: 20,
        justifyContent: 'center',
    },
    buttonText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: '#2ecc71'
    },
    desc: {
        margin: 20,
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: 'rgb(44, 62, 80)',
        lineHeight: 24
    }
});
