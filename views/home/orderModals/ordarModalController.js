import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Animated,
    Alert,
    Dimensions,
    Image
} from 'react-native';
import { PlaceModalFrom } from './placeModalFrom'
import { PlaceModalTo } from './placeModalTo'
import { connect } from 'react-redux'
import { FareModal } from './fareModal'
import { CommentModal } from './commentModal'
import API from '../../../networking/api'
import { GoogleApi } from '../../../networking/google.api'
import LoadingButton from '../../../components/loadigButton/loadingButton'

class ModalControllerClass extends Component {
    alphabet = ["b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    api = new API()
    controllerAnim = new Animated.Value(15)
    placeAnimFrom = new Animated.Value(-293)
    placeAnimTo = new Animated.Value(-293)
    fareAnim = new Animated.Value(-220)
    commentAnim = new Animated.Value(-220)
    endPointIndex = 0

    constructor(props) {
        super(props)
        this.state = {
            to: [{
                short_name: '',
            }],
            fare: '',
            wishes: {
                comment: '',
                child: 0,
                passengers: 0
            },
            loading: false,
            time: '',
            price: ''
        }

    }

    componentDidMount() {
        this.props.onRef(this)
    }

    _renderValueText(to) {
        return to.address ?
            true :
            false
    }

    savePlaceFrom(from) {
        this.props.dispatch({ type: 'SET_FROM', value: from })
        this.props.animateToCoordinate(from)
    }

    savePlaceTo(to) {

        let arr = this.state.to
        arr[this.endPointIndex] = to
        this.getNewTripInfo(arr)
        this.setState({
            to: arr
        })
        this.props.setTo(arr.filter(item => item.latitude))
    }

    commentAnimation(controller, commentAnimValue, save, wishes) {
        Animated.parallel([
            Animated.timing(this.controllerAnim, {
                toValue: controller,
                duration: 200
            }),
            Animated.timing(this.commentAnim, {
                toValue: commentAnimValue,
                duration: 200
            })
        ]).start(() => {
            if (save) {
                this.setState({
                    wishes: wishes
                })
            }
        });
    }

    fareAnimation(controller, fareAnimValue, save, fare) {
        Animated.parallel([
            Animated.timing(this.controllerAnim, {
                toValue: controller,
                duration: 200
            }),
            Animated.timing(this.fareAnim, {
                toValue: fareAnimValue,
                duration: 200
            })
        ]).start(() => {
            if (save) {
                this.setState({
                    fare: fare
                })
            }
        });
    }

    placeAnimationFrom(controller, place, save, from) {
        Animated.parallel([
            Animated.timing(this.controllerAnim, {
                toValue: controller,
                duration: 200
            }),
            Animated.timing(this.placeAnimFrom, {
                toValue: place,
                duration: 200
            })
        ]).start(() => {
            if (save) {
                this.savePlaceFrom(from)
            }
        });
    }

    placeAnimationTo(controller, place, save, to) {
        Animated.parallel([
            Animated.timing(this.controllerAnim, {
                toValue: controller,
                duration: 200
            }),
            Animated.timing(this.placeAnimTo, {
                toValue: place,
                duration: 200
            })
        ]).start(() => {
            if (save) {
                this.savePlaceTo(to)
            }
        });
    }

    setRide() {
        this.setState({
            loading: true
        })
        if (this.state.to[0].address) {
            let requestBody = {
                location_from_lat: this.props.map.from.latitude,
                location_from_lng: this.props.map.from.longitude,
                location_to_lat: this.state.to[0].latitude,
                location_to_lng: this.state.to[0].longitude,
            }
            GoogleApi.getDistance(requestBody).then((info) => {
                console.log('info');
                console.log(info);

                let distance = info.rows[0].elements[0].distance.value
                let duration = info.rows[0].elements[0].duration.value

                let destinations = this.state.to.filter((item) => item.latitude)
                destinations = destinations.map((item, i) => {
                    return {
                        lat: item.latitude,
                        lng: item.longitude,
                        address: item.address
                    }
                })
                let bodyData = {
                    address_from: this.props.map.from.address,
                    address_to: this.state.to[0].address,
                    cost: Number(this.state.fare),
                    customer: this.props.user.info._id,
                    children_qty: this.state.wishes.child,
                    comment: this.state.wishes.comment,
                    location_from_lat: this.props.map.from.latitude,
                    location_from_lng: this.props.map.from.longitude,
                    location_to_lat: this.state.to[0].latitude,
                    location_to_lng: this.state.to[0].longitude,
                    distance: distance,
                    duration: duration,
                    destinations: JSON.stringify(destinations)
                }
                this.api.drivings('POST', bodyData).then((res) => {
                    console.log(res);
                    if (res._status == 'OK') {
                        this.setState({
                            loading: false
                        })
                        this.btt.stop()
                        this.props.goToMap(res)
                        // Alert.alert(
                        //     '',
                        //     'Order registered !',
                        //     [
                        //         { text: 'OK', onPress: () => { }, style: 'cancel' },
                        //     ],
                        //     { cancelable: false }
                        // )
                    }
                    // this.reset()
                })
                    .catch((error) => {
                        console.log(error);
                        this.setState({
                            loading: false
                        })
                        this.btt.stop()
                        Alert.alert(
                            '',
                            'Server Error !',
                            [
                                { text: 'Close', onPress: () => { }, style: 'cancel' },
                            ],
                            { cancelable: false }
                        )
                    })
            })
                .catch((error) => {
                    console.log(error);

                })

        }
        else {
            this.setState({
                loading: false
            })
            this.btt.stop()
            Alert.alert(
                '',
                'Select to point !',
                [
                    { text: 'OK', onPress: () => { }, style: 'cancel' },
                ],
                { cancelable: false }
            )
        }
    }

    reset() {
        this.setState({
            to: [{
                short_name: '',
            }],
            fare: '',
            wishes: {
                comment: '',
                child: 0,
                passengers: 0
            },
            time: '',
            duration: ''
        })
        this.props.setTo([])
        //this.props.animateToCoordinate(this.props.map.from)
    }

    getNewTripInfo(to) {
        let requestLocationsUrl = ''

        for (let i = 0; i < to.length; i++) {
            if (i == 0) {
                requestLocationsUrl += to[i].latitude + ',' + to[i].longitude
            }
            else {
                requestLocationsUrl += '|' + to[i].latitude + ',' + to[i].longitude
            }
        }

        console.log('requestLocationsUrl');

        console.log(requestLocationsUrl);


        let requestBody = {
            location_from_lat: this.props.map.from.latitude,
            location_from_lng: this.props.map.from.longitude,
            // location_to_lat: to.latitude,
            // location_to_lng: to.longitude,
            destinations: requestLocationsUrl
        }
        GoogleApi.getDistanceMultiple(requestBody).then((info) => {
            console.log('info');
            console.log(info);
            let distance = 0
            let duration = 0

            for (let i = 0; i < info.rows[0].elements.length; i++) {
                distance += info.rows[0].elements[i].distance.value
                duration += info.rows[0].elements[i].duration.value
            }


            this.api.getPrice(distance).then((price) => {
                console.log(price);
                this.setState({
                    price: price,
                    time: duration
                })

            })
                .catch((error) => {
                    console.log(error);

                })
        })
            .catch((error) => {
                console.log(error);

            })
    }

    formatTime() {
        let hours = Math.floor(this.state.time / 60 / 60)
        let minutes = Math.floor((this.state.time / 60) % 60)
        return hours == 0 ? ` ~${minutes} min` : ` ~${hours}h ${minutes} min`
    }

    _renderInfoItem() {
        return this.state.time == '' ? null :
            (<View style={styles.infoItemRow}>
                <Text style={[styles.infoItemText,
                {
                    flex: 1,
                    textAlign: "center",
                    fontSize: 14,
                }]}>
                    Ride-hailing apps average standard price is {'\n'}
                    {'₦ ' + this.state.price.toFixed(2)}{'\n'}
                    Destination travel time: {this.formatTime()}
                </Text>
            </View>)
    }

    _renderAddOrEmpty() {
        return this.state.to.length < 3 ?
            <TouchableOpacity
                onPress={() => {
                    let arr = this.state.to
                    arr.push({
                        short_name: '',
                    })
                    this.setState({
                        to: arr
                    })
                }}
                activeOpacity={0.8}
                style={styles.toItemIconContainer}>
                <Image
                    source={require('../../../assets/icons/add.png')}
                    style={styles.icon} />
            </TouchableOpacity> :
            null
    }

    _renderAddButton(index) {
        return index == 0 ? this._renderAddOrEmpty() :
            <TouchableOpacity
                onPress={() => {
                    let arr = this.state.to
                    arr.splice(index, 1)
                    console.log('OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO');

                    console.log(arr);

                    this.setState({
                        to: arr
                    })
                    this.getNewTripInfo(arr)
                    this.props.setTo(arr.filter(item => item.latitude))
                }}
                activeOpacity={0.8}
                style={styles.toItemIconContainer}>
                <Image
                    source={require('../../../assets/icons/close.png')}
                    style={styles.icon} />
            </TouchableOpacity>
    }

    _renderToItem() {
        return this.state.to.map((item, index) => (<View
            key={index}
            style={styles.itemContainer}>
            <View style={styles.pointIconContainer}>
                <View style={[styles.pointLine, { top: 0 }]} />
                <View style={styles.pointIcon}>
                    <Text style={styles.pointIconText}>
                        {this.alphabet[index].toLocaleUpperCase()}
                    </Text>
                </View>
                {index == this.state.to.length - 1 ?
                    null :
                    <View style={[styles.pointLine, { bottom: 0 }]} />
                }
            </View>
            <TouchableOpacity
                onPress={() => {
                    this.endPointIndex = index
                    this.placeAnimationTo(-400, 15, false, {})
                }}
                activeOpacity={0.5}
                style={styles.item}>
                <Text
                    numberOfLines={1}
                    style={[
                        styles.itemText,
                        item.address ?
                            { color: 'rgb(44, 62, 80)' } :
                            { color: 'rgb(189,195,199)' }
                    ]}>
                    {item.address ?
                        item.short_name :
                        'To'
                    }
                </Text>
                {this._renderAddButton(index)}
            </TouchableOpacity>
        </View>))
    }

    render() {
        if (this.props.user.activeDriving) {
            return null
        }
        return (<View>
            <Animated.View
                style={[styles.itemsContainer, {
                    bottom: this.controllerAnim
                }]}>
                <View style={styles.itemContainer}>
                    <View style={styles.pointIconContainer}>
                        <View style={styles.pointIcon}>
                            <Text style={styles.pointIconText}>
                                A
                </Text>
                        </View>
                        <View style={[styles.pointLine, { bottom: 0 }]} />
                    </View>
                    <TouchableOpacity
                        onPress={() => {
                            this.placeAnimationFrom(-400, 15, false, {})
                        }}
                        activeOpacity={0.5}
                        style={styles.item}>
                        <Text
                            numberOfLines={1}
                            style={[
                                styles.itemText,
                                { color: 'rgb(44, 62, 80)' }
                            ]}>
                            {this.props.map.from.short_name}
                        </Text>
                    </TouchableOpacity>
                </View>
                {this._renderToItem()}
                {this._renderInfoItem()}
                <TouchableOpacity
                    onPress={() => {
                        this.fareAnimation(-400, 15, false, '')
                    }}
                    activeOpacity={0.5}
                    style={styles.item}>
                    <Text
                        numberOfLines={1}
                        style={[
                            styles.itemText,
                            this.state.fare.length > 0 ?
                                { color: 'rgb(44, 62, 80)' } :
                                { color: 'rgb(189,195,199)' }
                        ]}>
                        <Text style={[styles.itemText, { color: '#2ecc71' }]}>
                            {'₦ '}
                        </Text>
                        {this.state.fare.length > 0 ?
                            this.state.fare :
                            'Offer your fare'
                        }
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.commentAnimation(-400, 15, false, '')
                    }}
                    activeOpacity={0.5}
                    style={styles.item}>
                    <Text
                        numberOfLines={1}
                        style={[
                            styles.itemText,
                            this.state.wishes.comment ?
                                { color: 'rgb(44, 62, 80)' } :
                                { color: 'rgb(189,195,199)' }
                        ]}>
                        {this.state.wishes.comment ?
                            this.state.wishes.comment :
                            'Comment and wishes'
                        }
                    </Text>
                </TouchableOpacity>
                <View style={[styles.buttonsRow, { marginBottom: 15, justifyContent: "center" }]}>
                    {/* <TouchableOpacity
                        onPress={() => {
                            console.log(this.props.map.from);

                            this.reset()
                        }}
                        activeOpacity={0.7}
                        style={styles.rideButton}>
                        <Text
                            style={styles.rideButtonText}>
                            Reset
                        </Text>
                    </TouchableOpacity> */}
                    <LoadingButton
                        ref={ref => this.btt = ref}
                        text='Request ride'
                        onPressCallback={() => {
                            this.setRide()
                        }}
                        disabled={this.state.loading}
                        width={(Dimensions.get('window').width * 47 / 100) - 30}
                    />
                    {/* <TouchableOpacity
                        onPress={() => {
                            this.setRide()
                        }}
                        activeOpacity={0.7}
                        style={styles.rideButton}>
                        <Text
                            style={styles.rideButtonText}>
                            Ride
                        </Text>
                    </TouchableOpacity> */}
                </View>
            </Animated.View>
            <PlaceModalFrom
                selectItem={(from) => {
                    this.savePlaceFrom(from)
                }}
                close={() => {
                    this.placeAnimationFrom(15, -293, false, {})
                }}
                done={(from) => {
                    this.placeAnimationFrom(15, -293, true, from)
                }}
                placeAnim={this.placeAnimFrom}
            />
            <PlaceModalTo
                selectItem={(to) => {
                    this.savePlaceTo(to)
                }}
                to={this.state.to}
                index={this.endPointIndex}
                close={() => {
                    this.placeAnimationTo(15, -293, false, {})
                }}
                done={(to) => {
                    this.placeAnimationTo(15, -293, true, to)
                }}
                placeAnim={this.placeAnimTo}
            />
            <FareModal
                close={() => {
                    this.fareAnimation(15, -220, false, '')
                }}
                done={(fare) => {
                    this.fareAnimation(15, -220, true, fare)
                }}
                fareAnim={this.fareAnim}
            />
            <CommentModal
                close={() => {
                    this.commentAnimation(15, -220, false, '')
                }}
                done={(wishes) => {
                    this.commentAnimation(15, -220, true, wishes)
                }}
                commentAnim={this.commentAnim}
                wishes={this.state.wishes}
            />
        </View>);
    }
}

export const ModalController = connect(
    ({ map, user }) => ({ map, user })
)(ModalControllerClass)

const styles = StyleSheet.create({
    itemsContainer: {
        position: 'absolute',
        minHeight: 260,
        paddingTop: 5,
        left: 15,
        right: 15,
        backgroundColor: '#fff',
        borderRadius: 4,
        elevation: 5,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 20,
        shadowOpacity: 1,
    },
    pointIconText: {
        fontSize: 12,
        color: '#fff',
        fontFamily: 'Roboto-Regular'
    },
    pointIconContainer: {
        marginLeft: 20,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    pointIcon: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#2ecc71',
        justifyContent: 'center',
        alignItems: 'center',
    },
    pointLine: {
        position: 'absolute',
        height: 7,
        width: 1,
        backgroundColor: '#bdc3c7'
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 48,
        width: '100%',
    },
    item: {
        flex: 1,
        flexDirection: 'row',
        height: 48,
        marginHorizontal: 20,
        borderBottomColor: '#2ecc71',
        borderBottomWidth: 2,
        alignItems: 'center',
    },
    itemText: {
        flex: 1,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
    },
    buttonsRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20
    },
    rideButton: {
        marginTop: 10,
        height: 40,
        width: '47%',
        backgroundColor: '#fff',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rideButtonText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: '#d80027',
    },
    infoItemRow: {
        marginHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5
    },
    infoItemCol: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    infoItemText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: 'rgb(44, 62, 80)'
    },
    toItemIconContainer: {
        height: '100%',
        width: 20,
        height: 20
    },
    icon: {
        width: 20,
        height: 20
    }
});
