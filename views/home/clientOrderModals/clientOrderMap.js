import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    BackHandler,
    TouchableOpacity,
    Alert,
    Linking,
    ImageBackground
} from 'react-native';
import MapView, { ProviderPropType, Marker, AnimatedRegion } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { connect } from 'react-redux'
import ClientProcessSlider from './clientProcessSlider'
import API from '../../../networking/api'
import { GoogleApi } from '../../../networking/google.api'
import { Rating, AirbnbRating } from 'react-native-ratings';
import ChangeOfferModal from '../../offerModals/changeOfferModal'
import OfferModal from '../../offerModals/offerModal'
import OfferMapModal from '../../offerModals/offerMapModal'
import Pulse from 'react-native-pulse';
var Sound = require('react-native-sound');

class ClientOrderMapClass extends Component {

    alphabet = ['a', "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    api = new API()
    API_KEY = "AIzaSyAP5rIF-LSllA_P74bw3tMZKxHHndcb9kY"
    sliderLength = Dimensions.get('window').width - 70
    canPlay = true

    constructor(props) {
        super(props)
        this.state = {
            // coordinate: new AnimatedRegion({
            //     latitude: 40,
            //     longitude: 40,
            // }),
            driver: null,
            trip: {},
            rotate: 0,
            changeOfferModalVisible: false,
            offerModalVisible: false,
            offerMapVisible: false,
            locationAddress: '',
            location_to_lat: null,
            location_to_lng: null,
            carColor: null,
            carModel: null,
            destinations: [],
            newDestinations: [],
            activePointIndex: 0,
            newPrice: '',
            point: {
                latitude: 0,
                longitude: 0,
                address: ""
            }
        }
        const { params } = this.props.navigation.state
        this.state.trip = params
        this.state.destinations = JSON.parse(params.destinations).map((item) => {
            return {
                latitude: item.lat,
                longitude: item.lng,
                address: item.address
            }
        })
        console.log('this.state.destinations');
        console.log(this.state.destinations);

    }

    componentWillMount() {
        //BackHandler.addEventListener('hardwareBackPress', () => true)
    }

    componentDidMount() {
        const { params } = this.props.navigation.state
        let fromCoords = {
            latitude: params.location_from_lat,
            longitude: params.location_from_lng
        }
            setTimeout(() => {
                this.map.fitToCoordinates(
                    [
                        fromCoords,
                        ...this.state.destinations
                    ],
                    {
                        edgePadding: { top: 140, right: 100, bottom: 600, left: 100 },
                        animated: true
                    }
                )
            }, 500)
        this.getTrip()
        this.setInterval = setInterval(() => {
            this.getTrip()
        }, 5000)

    }

    componentWillUnmount() {
        clearInterval(this.setInterval)
    }

    getOffers() {
        const { params } = this.props.navigation.state
        let bodyData = {}
        if (params.customer == this.props.user.info._id) {
            bodyData = {
                confirmed_by_customer: false,
                confirmed_by_driver: true,
                driving: params._id
            }
        }
        else {
            bodyData = {
                confirmed_by_customer: true,
                confirmed_by_driver: false,
                driving: params._id
            }
        }
        this.api.getDrivingsOffers(bodyData, 1).then((offers) => {
            console.log(offers);

            if (offers._items.length > 0) {
                console.log('||||||||||offers|||||||||||||');
                this.setState({
                    offerModalVisible: true,
                    offer: offers._items[0]
                })
            }
        })
            .catch((error) => {
                console.log(error);

            })
    }


    getPrice(destinations) {
        let to = destinations.filter(item => item.address != '')
        let requestLocationsUrl = ''

        for (let i = 0; i < to.length; i++) {
            if (i == 0) {
                requestLocationsUrl += to[i].latitude + ',' + to[i].longitude
            }
            else {
                requestLocationsUrl += '|' + to[i].latitude + ',' + to[i].longitude
            }
        }

        console.log('requestLocationsUrl');

        console.log(requestLocationsUrl);


        let requestBody = {
            location_from_lat: this.state.trip.location_from_lat,
            location_from_lng: this.state.trip.location_from_lng,
            // location_to_lat: to.latitude,
            // location_to_lng: to.longitude,
            destinations: requestLocationsUrl
        }
        GoogleApi.getDistanceMultiple(requestBody).then((info) => {
            console.log('info');
            console.log(info);
            let distance = 0
            //let duration = 0

            for (let i = 0; i < info.rows[0].elements.length; i++) {
                distance += info.rows[0].elements[i].distance.value
                //duration += info.rows[0].elements[i].duration.value
            }


            this.api.getPrice(distance).then((price) => {
                console.log('price');
                console.log(price.toString());
                this.setState({
                    newDestinations: destinations,
                    offerMapVisible: false,
                    newPrice: price.toFixed().toString()
                })
            })
                .catch((error) => {
                    console.log(error);

                })
        })
            .catch((error) => {
                console.log(error);

            })
    }

    getTrip() {
        this.api.drivings('GET', null, null, this.state.trip._id).then((res) => {
            console.log(res);
            if (res.status == 'canceled') {
                this.props.dispatch({ type: 'SET_DRIVING', value: false })
                this.props.navigation.navigate('home')
                Alert.alert(
                    '',
                    'Trip canceled!',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            }
            else if (res.status == 'complete') {
                this.props.navigation.navigate('home', { rating: true })
            }
            else {
                let destinations = JSON.parse(res.destinations).map((item) => {
                    return {
                        latitude: item.lat,
                        longitude: item.lng,
                        address: item.address
                    }
                })
                this.setState({
                    trip: res,
                    destinations: destinations
                })
                if (!this.state.offerModalVisible && !this.state.changeOfferModalVisible) {
                    this.getOffers()
                }
                if (res.driver && this.state.driver == null) {
                    this.api.getUserById(res.driver).then((driverRes) => {

                        this.api.getColorById(driverRes.driver_car_color).then((color) => {


                            this.api.getCarModalById(driverRes.driver_car_model).then((model) => {

                                this.setState({
                                    driver: driverRes,
                                    carColor: color,
                                    carModel: model
                                })

                            })
                                .catch((error) => {
                                    console.log(error);

                                })
                        })
                            .catch((error) => {
                                console.log(error);

                            })
                    })
                        .catch((error) => {
                            console.log(error);

                        })
                }
                if (res.status == 'driver_arrived') {
                    if (this.canPlay) {
                        this.canPlay = false
                        var sound = new Sound(require('../../../assets/sounds/sound.mp3'), (error) => {
                            if (error) {
                                console.log('failed to load the sound', error);
                                return
                            }
                            sound.play((success) => {
                                if (success) {
                                    console.log('successfully finished playing');
                                } else {
                                    console.log('playback failed due to audio decoding errors');
                                }
                            });
                        });
                        Alert.alert(
                            '',
                            'Your Driver have Arrive',
                            [
                                { text: 'Close', onPress: () => { }, style: 'cancel' },
                            ],
                            { cancelable: false }
                        )
                    }
                }
            }
        })
            .catch((error) => {
                console.log(error);

            })
    }

    changeStatus(status) {
        console.log('+++++++++++++++++++++++++++++++++++++++++');

        console.log(this.state.trip);
        this.api.drivings('GET', null, null, this.state.trip._id).then((res) => {
            console.log(res);
            let bodyData = {
                status: status,
            }
            this.api.drivings('PATCH', bodyData, 0, res._id, res._etag)
                .then((res2) => {
                    this.props.dispatch({ type: 'SET_DRIVING', value: false })
                    console.log(res2);
                    this.props.navigation.navigate('home')
                    // this.props.goBack()
                })
                .catch((error) => {
                    console.log(error);
                })
        })
            .catch((error) => {
                console.log(error);

            })

    }

    animateMarker = (newCoordinate, distance) => {
        this.markerRotate(newCoordinate)
        this.setState({
            distance: distance
        })
        // const { coordinate } = this.state;
        // console.log(coordinate);
        // console.log(newCoordinate);
        // coordinate.timing(newCoordinate, 5000).start();
    }

    markerRotate(coordinate) {
        if (this.coords) {
            let lat1 = this.coords.latitude
            let long1 = this.coords.longitude

            let lat2 = coordinate.latitude
            let long2 = coordinate.longitude

            let dLon = (long2 - long1);

            let y = Math.sin(dLon) * Math.cos(lat2);
            let x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

            let brng = Math.atan2(y, x);

            brng = brng * 180 / Math.PI;
            brng = (brng + 360) % 360;
            brng = 360 - brng;
            this.coords = {
                latitude: lat2,
                longitude: long2
            }
            console.log('brng');

            console.log(brng);
            this.setState({
                rotate: brng
            })
        }
        else {
            console.log('else');

            this.coords = coordinate
        }
    }

    _renderMarker(coords) {
        return (<Marker
            style={{
                alignItems: 'center',
            }}
            coordinate={coords}
        >
            <Image
                style={{
                    width: 17,
                    height: 25.6
                }}
                source={require('../../../assets/images/marker2.png')}
            />
            <View style={styles.markerDot} />
        </Marker>)
    }

    _renderToMarkers() {
        return this.state.destinations.length > 0 ?
            this.state.destinations.map((item, i) => (<Marker
                key={i}
                coordinate={{
                    latitude: item.latitude,
                    longitude: item.longitude,
                }}
            >
                <View style={styles.pointIcon}>
                    <Text style={styles.pointIconText}>
                        {this.alphabet[i].toLocaleUpperCase()}
                    </Text>
                </View>
            </Marker>)) :
            null
    }

    _renderDriverMarker() {
        if (!this.state.trip.driver_location_lat) {
            return null
        }
        return (<Marker.Animated
            style={{
                alignItems: 'center',
            }}
            anchor={{ x: 0.5, y: 0.5 }}
            coordinate={{
                latitude: this.state.trip.driver_location_lat,
                longitude: this.state.trip.driver_location_lng,
            }}
        >
            <View style={{
                height: 50,
                width: 50,
                alignItems: 'center',
                justifyContent: 'center',
                transform: [{ rotate: this.state.rotate + 'deg' }]
            }}>

                <Image
                    style={{
                        height: 42,
                        width: 20,
                    }}
                    source={require('../../../assets/images/car.png')}
                />
            </View>
        </Marker.Animated>)
    }

    _renderDriverInfo() {
        return this.state.driver == null ? null :
            (<View><View style={styles.timeRow}>
                <View
                    style={styles.photoContainer}>
                    <Image
                        style={styles.image}
                        source={this.state.driver.avatar ?
                            { uri: 'data:image/png;base64,' + this.state.driver.avatar } :
                            require('../../../assets/images/1.png')}
                    />
                </View>
                <View style={{
                    flex: 1,
                    marginLeft: 15
                }}>
                    <View style={{
                        width: 83
                    }}>
                        {/* <AirbnbRating
                            defaultRating={4}
                            size={11}
                            isDisabled={true}
                            showRating={false} /> */}
                    </View>
                    <Text style={styles.priceText}>
                        {this.state.driver.firstname + ' ' + this.state.driver.lastname}
                    </Text>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 10
                    }}>
                        <View style={[styles.colorDot, this.state.carColor.code == null ? { backgroundColor: '#fff' } : { backgroundColor: this.state.carColor.code }]} />
                        <Text
                            numberOfLines={1}
                            style={styles.carBrandText}>
                            {this.state.carModel == null ? '' : this.state.carModel.title}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    onPress={() => {
                        const url = 'tel:+' + this.state.driver.phone
                        Linking.openURL(url)
                    }}
                    activeOpacity={0.8}
                    style={styles.phoneContainer}>
                    <Image
                        style={{
                            zIndex: 3,
                            position: 'absolute',
                            width: 20,
                            height: 20
                        }}
                        source={require('../../../assets/icons/phone.png')}
                    />
                </TouchableOpacity>
            </View>
                <View style={[styles.silverLiner, { marginBottom: 15 }]} />
            </View>)
    }

    formatTime() {
        let hours = Math.floor(this.state.trip.duration / 60 / 60)
        let minutes = Math.floor((this.state.trip.duration / 60) % 60)
        return hours == 0 ? ` ~${minutes} min` : ` ~${hours}h ${minutes} min`
    }

    _renderRideItem() {
        return (<View style={[styles.inPlaceItem, { paddingTop: 24 }]}>
            {this._renderDriverInfo()}
            <Text style={styles.itemTitle}>
                {this.state.trip.address_to}
            </Text>
            <View>
                <ClientProcessSlider
                    animateMarker={this.animateMarker}
                    trip={this.state.trip}
                />
                <View style={styles.disabledView} />
            </View>


            <View style={styles.timeRow2}>
                <Text style={[styles.itemTitle,
                {
                    flex: 1,
                    textAlign: "center",
                    fontSize: 14,
                }]}>
                    Ride-hailing apps average standard price is {'₦ ' + this.state.trip.cost.toFixed(2)}{'\n'}
                    Destination travel time: {this.formatTime()} {(this.state.distance / 1000).toFixed(1) + 'km'}
                </Text>
                {this.state.trip.status == 'driver_found' ?
                    <Text style={[styles.itemTitle,
                    {
                        flex: 1,
                        textAlign: "center",
                        fontSize: 14,
                    }]}>
                        The driver will arrive in {this.state.trip.eta_driver_time} minutes
                </Text> : null}
            </View>

            <View style={styles.silverLiner} />
            <View style={styles.buttonsRow}>
                <TouchableOpacity
                    onPress={() => {
                        this.changeStatus('canceled')
                    }}
                    activeOpacity={0.8}
                    style={[styles.itemButton, { backgroundColor: '#fff' }]}
                >
                    <Text style={[styles.priceText, { color: '#d80027' }]}>
                        Cancel
                </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    disabled={this.state.trip.status != 'driver_found'}
                    onPress={() => {
                        this.setState({
                            newDestinations: [...this.state.destinations],
                            changeOfferModalVisible: true,
                            newPrice: ''
                        })
                    }}
                    activeOpacity={0.8}
                    style={[styles.itemButton, this.state.trip.status == 'driver_found' ? null : { backgroundColor: 'silver' }]}
                >
                    <Text style={[styles.priceText, { color: '#fff' }]}>
                        Change
                </Text>
                </TouchableOpacity>
            </View>
        </View>)
    }

    _renderSearchDriverContent() {
        console.log('this.state.trip');

        console.log(this.state.trip);

        return !this.state.trip.driver ?
            (<ImageBackground
                source={require('../../../assets/images/background.png')}
                style={styles.driverSearchContainer}
            >
                <View style={styles.searchDriverHeader}>
                    <Text style={styles.searchDriverTitle}>
                        Searching Driver
                </Text>
                    <TouchableOpacity
                        onPress={() => {
                            this.changeStatus('canceled')
                        }}
                    >
                        <Image
                            style={{
                                width: 20,
                                height: 20
                            }}
                            source={require('../../../assets/icons/close.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',

                    }}
                >
                    <Pulse color='#2fcc71' numPulses={8} diameter={400} speed={20} duration={2000} />
                    <Image
                        style={{
                            position: 'absolute',
                            width: 30,
                            height: 30,
                            tintColor: '#fff'
                        }}
                        source={require('../../../assets/icons/search.png')}
                    />
                </View>
            </ImageBackground>) :
            null
    }

    componentWillUpdate() {

    }

    render() {
        console.log('this.state.destinations');

        console.log(this.state.destinations);

        const { params } = this.props.navigation.state
        let fromCoords = {
            latitude: params.location_from_lat,
            longitude: params.location_from_lng
        }
        let toCoords = {
            latitude: params.location_to_lat,
            longitude: params.location_to_lng
        }

        return (<View style={styles.content}>
            <View style={styles.header}>
                <Text style={styles.headerTitle}>
                    City
                </Text>
            </View>
            <MapView
                ref={ref => this.map = ref}
                style={styles.map}
                initialRegion={{
                    ...fromCoords,
                    latitudeDelta: 0.06,
                    longitudeDelta: 0.06,
                }}>
                <MapViewDirections
                    strokeWidth={6}
                    strokeColor='#7f2c26'
                    origin={fromCoords}
                    waypoints={this.state.destinations}
                    destination={this.state.destinations[this.state.destinations.length - 1]}
                    apikey={this.API_KEY}
                />
                {this._renderMarker(fromCoords)}
                {this._renderToMarkers()}
                {this._renderDriverMarker()}
                {false ? <Marker
                    style={{
                        alignItems: 'center',
                    }}
                    anchor={{ x: 0.5, y: 0.5 }}
                    coordinate={{
                        latitude: this.props.map.driverLocation.latitude,
                        longitude: this.props.map.driverLocation.longitude
                    }}
                >
                    <View style={{
                        height: 50,
                        width: 50,
                        alignItems: 'center',
                        justifyContent: 'center',
                        transform: [{ rotate: this.state.rotate + 'deg' }]
                    }}>

                        <Image
                            style={{
                                height: 42,
                                width: 20,
                            }}
                            source={require('../../../assets/images/car.png')}
                        />
                    </View>
                </Marker> : null}
            </MapView>
            <View style={{
                position: 'absolute',
                left: 15,
                right: 15,
                bottom: 15,
                zIndex: 1
            }}>
                {this._renderRideItem()}
            </View>
            <ChangeOfferModal
                price={this.state.newPrice}
                changePrice={(p) => {
                    this.setState({
                        newPrice: p
                    })
                }}
                location={{
                    address: this.state.locationAddress,
                    location_to_lat: this.state.location_to_lat,
                    location_to_lng: this.state.location_to_lng
                }}
                destinations={this.state.newDestinations}
                openMap={(i) => {
                    console.log('-***********-*-------------------------**********');

                    console.log(this.state.newDestinations[i]);

                    this.setState({
                        offerMapVisible: true,
                        activePointIndex: i,
                        point: this.state.newDestinations[i]
                    })
                }}
                deletePoint={(i) => {
                    let destinations = this.state.newDestinations
                    destinations.splice(i, 1)
                    this.setState({
                        newDestinations: [...destinations],
                    })
                }}
                addPoint={() => {
                    let destinations = this.state.newDestinations
                    destinations.push({
                        latitude: 0,
                        longitude: 0,
                        address: ""
                    })
                    this.setState({
                        newDestinations: destinations
                    })
                }}
                close={(e) => {
                    if (e) {
                        this.setState({
                            changeOfferModalVisible: false,
                            destination: this.state.newDestinations
                        })
                    }
                    else {
                        this.setState({
                            changeOfferModalVisible: false,
                        })
                    }

                }}
                user={this.state.driver}
                visible={this.state.changeOfferModalVisible}
                trip={this.state.trip}
            />
            {this.state.offer ? <OfferModal
                modalVisible={this.state.offerModalVisible}
                offer={this.state.offer}
                user={this.state.driver}
                trip={this.state.trip}
                goBack={() => {
                    this.setState({
                        offerModalVisible: false
                    })
                }}
            /> : null}
            <OfferMapModal
                done={(data) => {
                    let destinations = this.state.newDestinations
                    destinations[this.state.activePointIndex] = data
                    this.getPrice(destinations)

                }}
                trip={this.state.trip}
                point={this.state.point}

                visible={this.state.offerMapVisible}
                close={() => {
                    this.setState({
                        offerMapVisible: false
                    })
                }}
            />
            {this._renderSearchDriverContent()}
        </View>);
    }
}

// OrderMap.propTypes = {
//     provider: ProviderPropType,
// };

export const ClientOrderMap = connect(
    ({ map, user }) => ({ map, user })
)(ClientOrderMapClass)

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#ece9e1'
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    header: {
        zIndex: 2,
        height: 38,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        fontFamily: 'Roboto-Bold',
        fontSize: 14,
        color: 'rgb(44, 62, 80)',
    },
    markerDot: {
        position: 'absolute',
        top: 3,
        height: 10,
        width: 10,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    inPlaceItem: {
        minHeight: 218,
        paddingHorizontal: 20,
        backgroundColor: '#fff',
        borderRadius: 4,
        elevation: 5,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 20,
        shadowOpacity: 1,
    },
    itemTitle: {
        // marginTop: 24,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    disabledView: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'transparent'
    },
    photoContainer: {
        height: 48,
        width: 48,
        borderRadius: 24,
        backgroundColor: 'silver'
    },
    priceText: {
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    timeRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15
    },
    timeRow2: {
        //flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15
    },
    silverLiner: {
        height: 1,
        backgroundColor: '#e5e3dd',
    },
    phoneContainer: {
        position: 'absolute',
        right: 0,
        backgroundColor: '#2ecc71',
        width: 36,
        height: 36,
        borderRadius: 24,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemButton: {
        marginTop: 15,
        alignSelf: 'center',
        height: 40,
        width: '40%',
        backgroundColor: '#2ecc71',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    timeTitle: {
        marginBottom: 10,
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: 'rgb(189, 195, 199)'
    },
    buttonsRow: {
        marginBottom: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    image: {
        flex: 1,
        width: '100%',
        borderRadius: 28
    },
    colorDot: {
        width: 16,
        height: 16,
        borderRadius: 8
    },
    carBrandText: {
        flex: 1,
        marginLeft: 5,
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: 'rgb(44, 62, 80)'
    },
    driverSearchContainer: {
        ...StyleSheet.absoluteFillObject,
        zIndex: 5,
        backgroundColor: '#fff'
    },
    searchDriverHeader: {
        height: 50,
        paddingHorizontal: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#ecf0f1',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    searchDriverTitle: {
        fontFamily: 'Roboto-Regular',
        fontSize: 20,
        color: 'rgb(44, 62, 80)'
    },
    pointIconText: {
        fontSize: 14,
        color: '#fff',
        fontFamily: 'Roboto-Regular'
    },
    pointIcon: {
        height: 22,
        width: 22,
        borderRadius: 10,
        backgroundColor: '#2ecc71',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
