import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ImageBackground,
    Dimensions,
    Animated,
    Keyboard,
    Alert,
    TextInput,
    Modal,
    ScrollView,
} from 'react-native';
import { connect } from 'react-redux'
import { Rating, AirbnbRating } from '../../../components/react-native-ratings';
import API from '../../../networking/api';

class DriverRattingClass extends Component {
    api = new API();
    ratingCount = 5;
    constructor(props) {
        super(props)
        this.state = {
            comment: "",
           
        }
    }

    setDriverRating(driverId, cutsomerId, rating, comment) {
        console.log('fff',driverId,cutsomerId,rating,driverId && cutsomerId && rating && comment);
        if (driverId && cutsomerId && rating ) {
            this.api.createDriverRating({ driver: driverId, customer: cutsomerId, rating: rating.toString(), comment:this.state.comment }).then((data) => {
                console.log(data);
                this.props.callBack();
            })
        }
    }

    componentWillMount() {
        console.log(this.props.driverInfo, this.props.customerInfo);
    }

    checkPropertyValue(object, key, returnValue = null) {
        return (object && object[key]) ? object[key] : returnValue;
    }

    render() {
        console.log("modaaal", this.props.modalVisible);
        
        return (
            <Modal
            animationType="fade"
            transparent={false}
            visible={this.props.modalVisible}
            onRequestClose={() => {   this.props.callBack()}}>
 
        <View style={styles.container}>
            <ScrollView>
            <ImageBackground
                source={require('../../../assets/images/background.png')}
                style={styles.container}>
                <View style={styles.content}>
                    <Text style={styles.title}>
                        Rate your trip
               </Text>

                    <Text style={[styles.desc, {marginTop: 18,}]}>
                        Evaluating, you improve
                        </Text> 
                        <Text style={styles.desc}>the quality of service.
               </Text>

                    <View style={styles.imgContent}>
                        <View style={{
                            width: 90,
                            height: 90,
                            borderRadius: 50,
                           
                        }}>
                            <Image
                                style={{
                                    flex: 1,
                                    width: '100%',
                                    borderRadius: 50
                                }}
                                source={this.props.user.info.avatar ?
                                    { uri: 'data:image/png;base64,' + this.props.user.info.avatar } :
                                    require('../../../assets/images/1.png')
                                  }
                            />
                        </View>
                    </View>
                    <View style={styles.ratingContent}>
                        <AirbnbRating
                            count={5}
                            showRating={false}
                            defaultRating={11}
                            size={27}
                            onFinishRating={(value) => {
                                this.ratingCount = value;
                                console.log('ratingValue', value);
                            }}
                        />
                    </View>

                    <TextInput
                        onChangeText={(comment) => {
                            this.setState({
                                comment: comment
                            })
                        }}
                        value={this.state.comment}
                        placeholder='Comment'
                        style={styles.input} />
                    <TouchableOpacity
                        style={styles.buttonContent}
                        onPress={() => {
                            console.log(this.props);
                            this.setDriverRating(this.checkPropertyValue(this.props.driverInfo, 'driver'), this.checkPropertyValue(this.props.customerInfo, 'customer'), this.ratingCount, this.state.comment);

                        }}

                    >
                        <Text style={styles.buttonText}>Leave Feedback</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.closeButton}
                        onPress={() => {
                            console.log('ffffff');
                            this.props.callBack();
                            
                         }
                        }
                    >
                        <Text style={styles.closeText}>Close</Text>
                    </TouchableOpacity>
                </View>

            </ImageBackground>
                        </ScrollView>
        </View>
</Modal>
        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content:{
        height: Dimensions.get("window").height
    },
    title: {
        alignSelf: "center",
        color: 'rgb(44, 62, 80)',
        fontSize: 20,
        fontFamily: 'Roboto-Regular',
        marginTop: 80,
    },
    desc: {
        alignSelf: "center",
        textAlign: "center",
        color: 'rgb(44, 62, 80)',
        fontSize: 18,
        fontFamily: 'Roboto-Regular',
        
    },
    imgContent: {
        marginTop: 20,
        alignItems: "center"
    },
    ratingContent: {
        marginTop: 15,
        alignSelf: "center"
    },
    comentContent: {
        // borderWidth: 1,
        alignSelf: "center",
        borderWidth: 1,
        width: "80%",
        height: 35,
        marginHorizontal: 20
        // marginTop: 20,
        // alignItems: "center",
        // justifyContent: "center"
    },
    input: {
        alignSelf: "center",
        width: "80%",
        borderBottomColor: '#2ecc71',
        borderBottomWidth: 2,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    buttonContent: {
        width: "50%",
        height: 45,
        borderWidth: 3,
        borderColor: "rgb(46, 204, 113)",
        alignSelf: "center",
        textAlign: "center",
        marginTop: 20,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center"
    },
    buttonText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
    },
    closeButton: {
        justifyContent: "center",
        alignItems: "center",
        marginTop: 16
    },
    closeText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 18,
        color: 'rgb(229, 227, 221)'
    }
})
export const DriverRatting = connect(
    ({ map, user }) => ({ map, user })
)(DriverRattingClass)