import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions,
    Animated,
    Keyboard,
    Modal,
    Alert,
    AsyncStorage
} from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import { ModalController } from './orderModals'
import { EnableLocation } from './orderModals'
import { connect } from 'react-redux'
import MapViewDirections from 'react-native-maps-directions';
import { GoogleApi } from '../../networking/google.api'
import { BarIndicator } from 'react-native-indicators';
import API from '../../networking/api'
import Ratting, { DriverRatting } from "./clientOrderModals/driverRatting"
import moment from 'moment'
var Sound = require('react-native-sound');
export var newTripInterval = null

export function changeInterval() {
    newTripInterval = null
    console.log(newTripInterval);
}

class HomeClass extends Component {

    modalController = null
    alphabet = ["b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    api = new API()
    marginAnim = new Animated.Value(Dimensions.get('window').height - 62)
    modalController = null
    API_KEY = "AIzaSyAP5rIF-LSllA_P74bw3tMZKxHHndcb9kY"
    touchMap = false

    constructor(props) {
        super(props)
        this.state = {
            to: [],
            modalControllerVisible: true,
            keyboardHeight: 0,
            modalVisible: false
        }

    }

    componentDidMount() {
        //console.log(':::::::::::::::componentDidMount');
        // if (newTripInterval == null && this.props.user.info.is_driver) {
        //     console.log('register watch trips');

        //     this.watchNewTrips()
        // }
        const { params } = this.props.navigation.state
        //console.log(params);
        if (params) {
            if (params.rating) {
                this.setState({
                    modalVisible: true
                })
            }
        }
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this.keyboardDidShowListener,
        );
        this.keyboardHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this.keyboardHideListener,
        );
        this.onGoingTrip({ driver: this.props.user.info._id })
        if (newTripInterval == null) {
            newTripInterval = setInterval(() => {
                if (!this.props.user.activeDriving && this.props.user.info._id) {
                    if (this.props.user.info.is_driver) {
                        this.getStandbleListByLocation()
                    }

                    this.onGoingTrip({ driver: this.props.user.info._id })
                }
            }, 5000)
        }
        this.onGoingTrip({ customer: this.props.user.info._id })
    }

    playSound() {
        var sound = new Sound(require('../../assets/sounds/sound.mp3'), (error) => {
            if (error) {
                console.log('failed to load the sound', error);
                return
            }
            sound.play((success) => {
                if (success) {
                    console.log('successfully finished playing');
                } else {
                    console.log('playback failed due to audio decoding errors');
                }
            });
        });
    }

    getStandbleListByLocation() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                let coords = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                }
                let bodyData = {
                    status: 'new',
                }
                this.api.getDrivingsByLocation(bodyData, 1, coords, this.props.user.info._id).then((res) => {
                    console.log(res);
                    console.log('watchNewTrips _created');
                    if (res._items.length > 0) {
                        let localTime = moment.utc(res._items[0]._created, "YYYY-DD-MM HH:mm:ss").local().format("YYYY-DD-MM HH:mm:ss")
                        console.log(localTime);

                        let splitArr = localTime.split(' ')
                        let date = splitArr[0].split('-')
                        let time = splitArr[1].split(':')
                        let dateObj = new Date(date[0], Number(date[2]) - 1, date[1], Number(time[0]), Number(time[1]), Number(time[2]))
                        let now = new Date()
                        console.log(now.getTime() - dateObj.getTime());
                        if (now.getTime() - dateObj.getTime() < 5000) {
                            Alert.alert(
                                '',
                                'You have new offers from passengers.',
                                [
                                    { text: 'Close', onPress: () => { }, style: 'cancel' },
                                ],
                                { cancelable: false }
                            )
                            this.playSound()
                        }
                    }
                })
                    .catch((error) => {
                        console.log(error);

                    })
            },
            (error) => {

            },
            { enableHighAccuracy: false }
        );
    }

    onGoingTrip(data) {
        this.getTrip('driver_found', data).then((res) => {


            if (res._items.length > 0) {
                this.props.dispatch({ type: 'SET_DRIVING', value: true })
                if (data.driver) {

                    this.props.navigation.navigate('orderMap', res._items[0])
                }
                else {
                    this.props.navigation.navigate('clientOrderMap', res._items[0])
                }
                return
            }
            this.getTrip('driver_arrived', data).then((res) => {
                if (res._items.length > 0) {
                    this.props.dispatch({ type: 'SET_DRIVING', value: true })
                    if (data.driver) {
                        this.props.navigation.navigate('orderMap', res._items[0])
                    }
                    else {
                        this.props.navigation.navigate('clientOrderMap', res._items[0])
                    }
                    return
                }
                this.getTrip('in_progress', data).then((res) => {
                    if (res._items.length > 0) {
                        this.props.dispatch({ type: 'SET_DRIVING', value: true })
                        if (data.driver) {
                            this.props.navigation.navigate('orderMap', res._items[0])
                        }
                        else {
                            this.props.navigation.navigate('clientOrderMap', res._items[0])
                        }
                        return
                    }
                    if (!data.driver) {
                        this.getTrip('new', data).then((res) => {
                            if (res._items.length > 0) {
                                this.props.dispatch({ type: 'SET_DRIVING', value: true })
                                if (data.driver) {
                                    this.props.navigation.navigate('orderMap', res._items[0])
                                }
                                else {
                                    this.props.navigation.navigate('clientOrderMap', res._items[0])
                                }
                            }
                            else {
                                this.props.dispatch({ type: 'SET_DRIVING', value: false })
                            }
                        })
                            .catch((error) => {
                                //console.log(error);
                            })
                    }
                })
                    .catch((error) => {
                        //console.log(error);
                    })
            })
                .catch((error) => {
                    //console.log(error);
                })
        })
            .catch((error) => {
                //console.log(error);
            })
    }

    getTrip(status, data) {
        bodyData = {
            status: status,
            ...data
        }

        return this.api.drivings('GET', bodyData, 1).then((res) => {
            return res
        })
            .catch((error) => {
                //console.log(error);
                this.props.dispatch({ type: 'SET_DRIVING', value: false })
                // Alert.alert(
                //     '',
                //     'Server Error !',
                //     [
                //         { text: 'Close', onPress: () => { }, style: 'cancel' },
                //     ],
                //     { cancelable: false }
                // )
            })


    }

    _renderLoading() {
        return this.props.user.activeDriving ?
            (<View style={styles.loadingContainer}>
                <BarIndicator
                    count={7}
                    color='#2ecc71' />
            </View>) :
            null
    }

    keyboardDidShowListener = e => {
        this.setState({
            keyboardHeight: e.endCoordinates.height,
        });
    };

    keyboardHideListener = () => {
        this.setState({
            keyboardHeight: 0,
        });
    };

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardHideListener.remove();
    }

    setTo = (to) => {
        this.setState({
            to: to
        })
        if (to.length > 0) {
            this.map.fitToCoordinates(
                [
                    {
                        latitude: this.props.map.from.latitude,
                        longitude: this.props.map.from.longitude
                    },
                    ...to
                ],
                {
                    edgePadding: { top: 140, right: 100, bottom: 750, left: 100 },
                    animated: true
                }
            )
        }
    }

    animateToCoordinate = (from) => {
        this.map.animateToRegion({
            latitude: from.latitude,
            longitude: from.longitude,
            latitudeDelta: 0.06,
            longitudeDelta: 0.06,
        }, 500)
    }

    marginAnimation(value) {
        Animated.timing(this.marginAnim, {
            toValue: value,
            duration: 100
        }).start(() => {

        })
    }

    goToMap = (data) => {
        this.api.drivings('GET', null, null, data._id).then((res) => {

            this.props.navigation.navigate('clientOrderMap', res)
        })
            .catch((error) => {
                //console.log(error);

            })
    }

    openRatingModal() {
        //console.log("?????", this.state.modalVisible);

        return <DriverRatting 
        modalVisible={this.state.modalVisible}
         callBack={this.ratingModalCallback}
          driverInfo={{ driver: this.props.user.info._id }} 
          customerInfo={{ customer: this.props.user.info._id }} />
    }
    ratingModalCallback = () => {
        this.props.navigation.setParams({ rating: false })
        this.setState({
            modalVisible: false
        })
    }
    _renderModalController() {
        return <Animated.View
            style={{
                marginTop: this.marginAnim,

            }}
        >
            <View style={{
                marginTop: -this.state.keyboardHeight
            }}>
                <ModalController
                    onRef={ref => this.modalController = ref}
                    goToMap={this.goToMap}
                    setTo={this.setTo}
                    animateToCoordinate={this.animateToCoordinate}
                />
            </View>
        </Animated.View>
    }

    _renderInicialMarker() {
        if (this.state.to.length > 0) {
            return null
        }
        let center = Dimensions.get('window').height / 2 - 46
        return <View style={{
            position: 'absolute',
            zIndex: 1,
            top: center,
            alignItems: 'center',
            alignSelf: 'center'
        }}>
            <Image
                source={require('../../assets/images/userMarker.png')}
                style={{
                    flex: 1,
                    width: 44,
                    height: 53.3
                }}
            />

        </View>
    }

    _renderFromMarker() {
        return this.state.to.length > 0 ? <Marker
            tracksViewChanges={false}
            anchor={{ x: 0.5, y: 0.87 }}
            coordinate={{
                latitude: this.props.map.from.latitude,
                longitude: this.props.map.from.longitude,
            }}
        >
            <View style={styles.pointIcon}>
                <Text style={styles.pointIconText}>
                    A
                    </Text>
            </View>
            {/* <View
                style={{
                    alignItems: 'center'
                }}
            >
                <Image
                    source={require('../../assets/images/userMarker.png')}
                    style={{
                        flex: 1,
                        width: 44,
                        height: 53.3,
                    }}
                />
            </View> */}
        </Marker> : null
    }

    _renderToMarker() {
        return this.state.to.length > 0 ?
            this.state.to.map((item, i) => (<Marker
                tracksViewChanges={false}
                key={i}
                coordinate={{
                    latitude: item.latitude,
                    longitude: item.longitude,
                }}
            >
                <View style={styles.pointIcon}>
                    <Text style={styles.pointIconText}>
                        {this.alphabet[i].toLocaleUpperCase()}
                    </Text>
                </View>
            </Marker>)) :
            null
    }

    _renderDirections() {
        let ways = this.state.to.map(item => item);

        let destination = ways.pop()

        return this.state.to.length > 0 ? (<MapViewDirections
            strokeWidth={3}
            strokeColor='#7f2c26'
            origin={{
                latitude: this.props.map.from.latitude,
                longitude: this.props.map.from.longitude,
            }}
            waypoints={ways}
            destination={destination}
            apikey={this.API_KEY}
        />) :
            null
    }
    async getCity() {
        try {
            let city = await AsyncStorage.getItem('city')

            return city
        }
        catch (error) {
            //console.log(error);

        }
    }

    setLocation(city) {
        GoogleApi.getCityLocation(city).then((res) => {

            this.animateToCoordinate({
                latitude: res.geometry.location.lat,
                longitude: res.geometry.location.lng,
            })
        })
            .catch((error) => {
                //console.log(error);

            })
    }

    navigateToMyLocation() {
        this.modalController.reset()
        navigator.geolocation.getCurrentPosition(
            (position) => {
                let coordinates = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                }
                this.animateToCoordinate(coordinates)
            },
            (error) => {
                console.log(error);
                this.getCity().then((city) => {
                    //console.log(city);

                    if (city != null) {
                        this.setLocation(city)
                    }
                    else {
                        this.animateToCoordinate({
                            latitude: this.props.map.from.latitude,
                            longitude: this.props.map.from.longitude,
                        })
                    }

                })
                    .catch((error) => {
                        //console.log(error);

                    })
            },
            { enableHighAccuracy: false }
        );
    }

    _renderLocationButton() {
        return this.props.config.enterLocation ? (<TouchableOpacity
            onPress={() => {
                this.navigateToMyLocation()
            }}
            activeOpacity={0.8}
            style={styles.myLocationButton}>
            <Image
                style={styles.locationImage}
                source={require('../../assets/icons/location.png')} />
        </TouchableOpacity>) : null
    }

    _renderEnableLocation() {
        if (this.props.user.activeDriving || this.props.config.enterLocation) {
            return null
        }
        else {
            return (<EnableLocation
                done={this.animateToCoordinate}
            />)
        }
    }

    render() {
        //console.log(this.props.user.info._id);

        return (<View style={styles.container}>
            {this._renderLocationButton()}
            <View style={styles.header}>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('DrawerOpen')
                    }}
                    activeOpacity={0.7}
                    style={styles.menuButton}>
                    <Image
                        style={{
                            height: 14,
                            width: 20
                        }}
                        source={require('../../assets/icons/menu.png')}
                    />
                </TouchableOpacity>

                <Text style={styles.headerTitle}>
                    {this.props.map.from.city}
                </Text>

            </View>
            <MapView
                scrollEnabled={this.props.config.enterLocation}
                onRegionChange={(e) => {
                    //console.log(e);

                    if (this.touchMap && this.state.modalControllerVisible && this.state.to.length == 0) {
                        this.state.modalControllerVisible = false
                        this.marginAnimation(Dimensions.get('window').height + 300)
                    }
                }}
                onTouchStart={() => {
                    console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
                    this.touchMap = true
                }}
                onRegionChangeComplete={(e) => {


                    if (!this.state.modalControllerVisible && this.state.to.length == 0) {

                        console.log('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb');
                        if (this.touchMap) {
                            GoogleApi.getPlace({
                                latitude: e.latitude,
                                longitude: e.longitude
                            }).then(responseJson => {
                                ////console.log(responseJson);

                                this.props.dispatch({
                                    type: 'SET_FROM',
                                    value: {
                                        latitude: e.latitude,
                                        longitude: e.longitude,
                                        short_name: responseJson[0].address_components[1].short_name,
                                        address: responseJson[0].formatted_address,
                                        latitudeDelta: 0.06,
                                        longitudeDelta: 0.06,
                                        city: responseJson[2].address_components[2] ? responseJson[2].address_components[2].long_name : responseJson[2].address_components[0].long_name
                                    }
                                })
                            }).catch((error) => {
                                //console.log(error);

                            })
                        }
                        this.touchMap = false
                        this.state.modalControllerVisible = true
                        this.marginAnimation(Dimensions.get('window').height - 62)
                    }
                }}
                ref={ref => this.map = ref}
                style={styles.map}
                initialRegion={{
                    latitude: this.props.map.from.latitude,
                    longitude: this.props.map.from.longitude,
                    latitudeDelta: 0.06,
                    longitudeDelta: 0.06,
                }}
            >
                {this._renderToMarker()}
                {this._renderFromMarker()}
                {this._renderDirections()}
                {/* <Marker
                    coordinate={{
                        latitude: this.props.map.from.latitude,
                        longitude: this.props.map.from.longitude,
                    }}
                /> */}
            </MapView>
            {this._renderEnableLocation()}
            {this.props.config.enterLocation ? this._renderModalController() : null}
            {this.props.config.enterLocation ? this._renderInicialMarker() : null}
            {this._renderLoading()}
            {this.state.modalVisible ? this.openRatingModal() : null}
        </View>);
    }
}

export const Home = connect(
    ({ map, user, config }) => ({ map, user, config })
)(HomeClass)

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        // flex: 1,
        // position: 'absolute',
        height: Dimensions.get('window').height
    },
    header: {
        zIndex: 2,
        height: 38,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        fontFamily: 'Roboto-Bold',
        fontSize: 14,
        color: 'rgb(44, 62, 80)',
    },
    menuButton: {
        position: 'absolute',
        left: 15,
        height: 30,
        width: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },
    itemsContainer: {
        position: 'absolute',
        bottom: 15,
        height: 255,
        left: 15,
        right: 15,
        backgroundColor: '#fff',
        borderRadius: 4,
        elevation: 5,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 20,
        shadowOpacity: 1,
    },
    item: {
        height: 48,
        marginHorizontal: 20,
        borderBottomColor: '#2ecc71',
        borderBottomWidth: 2,
        justifyContent: 'center',
    },
    itemText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
    },
    rideButton: {
        marginTop: 10,
        height: 40,
        marginHorizontal: '30%',
        backgroundColor: '#2ecc71',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rideButtonText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: '#fff',
    },
    loadingContainer: {
        zIndex: 2,
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.2)',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    pointIconText: {
        fontSize: 14,
        color: '#fff',
        fontFamily: 'Roboto-Regular'
    },
    pointIcon: {
        height: 22,
        width: 22,
        borderRadius: 10,
        backgroundColor: '#2ecc71',
        justifyContent: 'center',
        alignItems: 'center',
    },
    myLocationButton: {
        zIndex: 2,
        backgroundColor: '#fff',
        borderColor: '#cbcbcb',
        borderWidth: 2,
        height: 50,
        width: 50,
        position: 'absolute',
        right: 10,
        borderRadius: 25,
        top: 48,
        justifyContent: 'center',
        alignItems: "center"
    },
    locationImage: {
        height: 30,
        width: 30,
        tintColor: '#cbcbcb'
    }
});
