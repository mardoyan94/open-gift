import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    BackHandler
} from 'react-native';
import MapView, { ProviderPropType, Marker, AnimatedRegion } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import ItemsController from './processItems/itemsController'
import { connect } from 'react-redux'
import API from '../../../networking/api'

class OrderMapClass extends Component {

    alphabet = ['a', "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    api=new API()
    API_KEY = "AIzaSyAP5rIF-LSllA_P74bw3tMZKxHHndcb9kY"
    sliderLength = Dimensions.get('window').width - 70

    constructor(props) {
        super(props)
        this.state = {
            // coordinate: new AnimatedRegion({
            //     latitude: 40,
            //     longitude: 40,
            // }),
            rotate: 0,
            trip: {},
            destinations: [],
        }
        const { params } = this.props.navigation.state
        this.state.trip = params
        this.state.destinations = JSON.parse(params.destinations).map((item) => {
            return {
                latitude: item.lat,
                longitude: item.lng,
                address: item.address
            }
        })
    }

    componentWillMount() {
        //BackHandler.addEventListener('hardwareBackPress', () => true)
    }

    componentDidMount() {
        let fromCoords = {
            latitude: this.state.trip.location_from_lat,
            longitude: this.state.trip.location_from_lng
        }

        setTimeout(() => {
            this.map.fitToCoordinates(
                [
                    fromCoords,
                    ...this.state.destinations
                ],
                {
                    edgePadding: { top: 140, right: 100, bottom: 600, left: 100 },
                    animated: true
                }
            )
        }, 500)
    }

    animateMarker = (newCoordinate) => {
 
        this.markerRotate(newCoordinate)
    }

    markerRotate(coordinate) {
        if (this.coords) {
            let lat1 = this.coords.latitude
            let long1 = this.coords.longitude

            let lat2 = coordinate.latitude
            let long2 = coordinate.longitude

            let dLon = (long2 - long1);

            let y = Math.sin(dLon) * Math.cos(lat2);
            let x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

            let brng = Math.atan2(y, x);

            brng = brng * 180 / Math.PI;
            brng = (brng + 360) % 360;
            brng = 360 - brng;
            this.coords = {
                latitude: lat2,
                longitude: long2
            }
            console.log('brng');

            console.log(brng);
            this.setState({
                rotate: brng
            })
        }
        else {
            console.log('else');

            this.coords = coordinate
        }
    }

    _renderMarker(coords) {
        return (<Marker
            style={{
                alignItems: 'center',
            }}
            coordinate={coords}
        >
            <Image
                style={{
                    width: 17,
                    height: 25.6
                }}
                source={require('../../../assets/images/marker2.png')}
            />
            <View style={styles.markerDot} />
        </Marker>)
    }

    _renderToMarkers() {
        return this.state.destinations.length > 0 ?
            this.state.destinations.map((item, i) => (<Marker
                key={i}
                coordinate={{
                    latitude: item.latitude,
                    longitude: item.longitude,
                }}
            >
                <View style={styles.pointIcon}>
                    <Text style={styles.pointIconText}>
                        {this.alphabet[i].toLocaleUpperCase()}
                    </Text>
                </View>
            </Marker>)) :
            null
    }

    _renderDriverMarker() {
        if (!this.state.trip.driver_location_lat) {
            return null
        }
        return (<Marker.Animated
            style={{
                alignItems: 'center',
            }}
            anchor={{ x: 0.5, y: 0.5 }}
            coordinate={{
                latitude:this.state.trip.driver_location_lat,
                longitude:this.state.trip.driver_location_lng,
            }}
        >
            <View style={{
                height: 50,
                width: 50,
                alignItems: 'center',
                justifyContent: 'center',
                transform: [{ rotate: this.state.rotate+'deg'}]
            }}>

            <Image
                    style={{
                        height: 42,
                        width: 20,
                    }}
                    source={require('../../../assets/images/car.png')}
                />
            </View>
        </Marker.Animated>)
    }

    goBack = () => {
        this.props.navigation.navigate('home')
    }

    render() {
        let fromCoords = {
            latitude: this.state.trip.location_from_lat,
            longitude: this.state.trip.location_from_lng
        }
        let toCoords = {
            latitude: this.state.trip.location_to_lat,
            longitude: this.state.trip.location_to_lng
        }

        return (<View style={styles.content}>
            <View style={styles.header}>
                <Text style={styles.headerTitle}>
                    City
                </Text>
            </View>
            <MapView
                ref={ref => this.map = ref}
                style={styles.map}
                initialRegion={{
                    ...fromCoords,
                    latitudeDelta: 0.06,
                    longitudeDelta: 0.06,
                }}>
                <MapViewDirections
                    strokeWidth={6}
                    strokeColor='#7f2c26'
                    origin={fromCoords}
                    waypoints={this.state.destinations}
                    destination={this.state.destinations[this.state.destinations.length - 1]}
                    apikey={this.API_KEY}
                />
                {this._renderMarker(fromCoords)}
                {this._renderToMarkers()}
                {this._renderDriverMarker()}
                {this.props.map.driverLocation.latitude ? <Marker
                    style={{
                        alignItems: 'center',
                    }}
                    anchor={{ x: 0.5, y: 0.5 }}
                    coordinate={{
                        latitude: this.props.map.driverLocation.latitude,
                        longitude: this.props.map.driverLocation.longitude
                    }}
                >
                    <View style={{
                        height: 50,
                        width: 50,
                        alignItems: 'center',
                        justifyContent: 'center',
                        transform: [{ rotate: this.state.rotate + 'deg' }]
                    }}>

                        <Image
                            style={{
                                height: 42,
                                width: 20,
                            }}
                            source={require('../../../assets/images/car.png')}
                        />
                    </View>
                </Marker> : null}
            </MapView>
            <ItemsController
                animateMarker={this.animateMarker}
                goBack={this.goBack}
                trip={this.state.trip}
                setTrip={(trip) => {
                    console.log('setTrip');
                    console.log(trip);
                    
                    this.setState({
                        trip: trip,
                        destinations: JSON.parse(trip.destinations).map((item) => {
                            return {
                                latitude: item.lat,
                                longitude: item.lng,
                                address: item.address
                            }
                        })
                    })
                }}
            />
        </View>);
    }
}

// OrderMap.propTypes = {
//     provider: ProviderPropType,
// };

export const OrderMap = connect(
    ({ map }) => ({ map })
)(OrderMapClass)

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#ece9e1'
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    header: {
        zIndex: 2,
        height: 38,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        fontFamily: 'Roboto-Bold',
        fontSize: 14,
        color: 'rgb(44, 62, 80)',
    },
    markerDot: {
        position: 'absolute',
        top: 3,
        height: 10,
        width: 10,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    pointIconText: {
        fontSize: 14,
        color: '#fff',
        fontFamily: 'Roboto-Regular'
    },
    pointIcon: {
        height: 22,
        width: 22,
        borderRadius: 10,
        backgroundColor: '#2ecc71',
        justifyContent: 'center',
        alignItems: 'center',
    }
});
