import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Dimensions,
    Animated,
    Alert,
    Linking
} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import CustomMarker from '../../costomMarker'
import { Rating, AirbnbRating } from 'react-native-ratings';
import API from '../../../../networking/api'
import { NavigationActions } from 'react-navigation'
import ProcessSlider from './processSlider'
import OfferModal from '../../../offerModals/offerModal'
import { connect } from 'react-redux'

class ItemsController extends Component {
    api = new API()
    sliderLength = Dimensions.get('window').width - 70
    inPlaceItemAnim = new Animated.Value(15)
    rideItemAnim = new Animated.Value(-300)
    inPlace = false

    constructor(props) {
        super(props)
        this.state = {
            inPlace: this.props.trip.status == 'driver_arrived' ? true : false,
            trip: this.props.trip,
            offerModalVisible: false,
            changeOfferModalVisible: false,
            client: null,
        }
        if (this.props.trip.status == 'in_progress') {
            this.inPlaceItemAnim.setValue(-300)
            this.rideItemAnim.setValue(15)
        }
    }

    getOffers() {
        let bodyData = {}
        if (this.props.trip.customer == this.props.user.info._id) {
            bodyData = {
                confirmed_by_customer: false,
                confirmed_by_driver: true,
                driving: this.props.trip._id
            }
        }
        else {
            bodyData = {
                confirmed_by_customer: true,
                confirmed_by_driver: false,
                driving: this.props.trip._id
            }
        }
        this.api.getDrivingsOffers(bodyData, 1).then((offers) => {
            console.log(offers);

            if (offers._items.length > 0) {
                console.log('||||||||||offers|||||||||||||');
                this.setState({
                    offerModalVisible: true,
                    offer: offers._items[0]
                })
            }
        })
            .catch((error) => {
                console.log(error);

            })
    }

    getTrip() {
        console.log('aaaaaaaaaaaaaaaaaaaaaaaaaa');

        console.log(this.state.trip);

        this.api.drivings('GET', null, null, this.state.trip._id).then((res) => {
            console.log(res);
            if (res.status == 'canceled') {
                this.props.goBack()
                Alert.alert(
                    '',
                    'Trip canceled!',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            }
            else {
                this.setState({
                    trip: res
                })
                this.props.setTrip(res)
                if (!this.state.offerModalVisible && !this.state.changeOfferModalVisible) {
                    this.getOffers()
                }
                if (res.customer && this.state.client == null) {
                    console.log('++++++++++++++++++++++++++++++++++++++');
                    this.api.getUserById(res.customer).then((res) => {
                        console.log(res);
                        this.setState({
                            client: res
                        })
                    })
                        .catch((error) => {
                            console.log(error);

                        })
                }
            }
        })
            .catch((error) => {
                console.log(error);

            })
    }

    cancelTrip() {
        console.log('---------------------------------------------');

        let bodyData = {
            status: 'new',
            driver: 'CLEAN'
        }
        console.log(bodyData);

        this.api.drivings('PATCH', bodyData, 0, this.state.trip._id, this.state.trip._etag)
            .then((res) => {
                console.log(res);

            })
            .catch((error) => {
                console.log(error);
            })
    }

    changeStatus(status, end) {
        console.log('+++++++++++++++++++++++++++++++++++++++++');

        console.log(this.state.trip);

        let bodyData = {
            status: status,
        }
        this.api.drivings('PATCH', bodyData, 0, this.state.trip._id, this.state.trip._etag)
            .then((res) => {
                console.log(res);
                if (!end) {
                    this.getTrip()
                }
            })
            .catch((error) => {
                console.log(error);
            })
    }

    inPlaceAnimation() {
        Animated.parallel([
            Animated.timing(this.inPlaceItemAnim, {
                toValue: -300,
                duration: 200
            }),
            Animated.timing(this.rideItemAnim, {
                toValue: 15,
                duration: 200
            })
        ]).start(() => {

        });
    }

    _renderClientInfo() {
        // return (<View style={styles.timeRow}>
        //     <TouchableOpacity
        //         onPress={() => {
        //             const url = 'tel:+' + this.state.client.phone
        //             Linking.openURL(url)
        //         }}
        //         activeOpacity={0.8}
        //         style={styles.photoContainer}>
        //         <Image
        //             style={styles.image}
        //             source={this.state.client.avatar ?
        //                 { uri: 'data:image/png;base64,' + this.state.client.avatar } :
        //                 require('../../../../assets/images/1.png')}
        //         />
        //         <View style={styles.phoneContainer}>
        //             <Image
        //                 style={{
        //                     zIndex: 3,
        //                     position: 'absolute',
        //                     width: 30,
        //                     height: 30
        //                 }}
        //                 source={require('../../../../assets/icons/phone.png')}
        //             />
        //         </View>
        //     </TouchableOpacity>
        //     <View style={{
        //         marginLeft: 15
        //     }}>
        //         <View style={{
        //             width: 83
        //         }}>
        //             <AirbnbRating
        //                 defaultRating={4}
        //                 size={11}
        //                 isDisabled={true}
        //                 showRating={false} />
        //         </View>
        //         <Text style={styles.priceText}>
        //             {this.state.client.firstname + ' ' + this.state.client.lastname}
        //         </Text>
        //     </View>
        //     <Text style={[styles.itemTitle,
        //     {
        //         flex: 1,
        //         textAlign: "right",
        //     }]}>
        //         <Text style={{ color: '#2ecc71' }}>
        //             {this.state.trip.cost.toFixed(2)}
        //         </Text>
        //         {' ₦'}
        //     </Text>
        // </View>)
        return this.state.client == null ? null :
            (<View><View style={styles.timeRow}>
                <View
                    style={styles.photoContainer}>
                    <Image
                        style={styles.image}
                        source={this.state.client.avatar ?
                            { uri: 'data:image/png;base64,' + this.state.client.avatar } :
                            require('../../../../assets/images/1.png')}
                    />
                </View>
                <View style={{
                    marginLeft: 15
                }}>
                    <View style={{
                        width: 83
                    }}>
                        {/* <AirbnbRating
                          defaultRating={4}
                          size={11}
                          isDisabled={true}
                          showRating={false} /> */}
                    </View>
                    <Text style={styles.priceText}>
                        {this.state.client.firstname + ' ' + this.state.client.lastname}
                    </Text>
                </View>
                <TouchableOpacity
                    onPress={() => {
                        const url = 'tel:+' + this.state.client.phone
                        Linking.openURL(url)
                    }}
                    activeOpacity={0.8}
                    style={styles.phoneContainer}>
                    <Image
                        style={{
                            zIndex: 3,
                            position: 'absolute',
                            width: 20,
                            height: 20
                        }}
                        source={require('../../../../assets/icons/phone.png')}
                    />
                </TouchableOpacity>
            </View>
                <View style={[styles.silverLiner, { marginBottom: 15 }]} />
            </View>)
    }

    formatTime() {
        let hours = Math.floor(this.state.trip.duration / 60 / 60)
        let minutes = Math.floor((this.state.trip.duration / 60) % 60)
        return hours == 0 ? ` ~${minutes} min` : ` ~${hours}h ${minutes} min`
    }

    _renderInPlaceItem() {
        return (<Animated.View style={[styles.inPlaceItem, { bottom: this.inPlaceItemAnim, paddingVertical: 20 }]}>
            {this._renderClientInfo()}
            <Text style={styles.itemTitle}>
                {this.state.trip.address_to}
            </Text>
            <View>
                <MultiSlider
                    selectedStyle={{
                        borderRadius: 2,
                        backgroundColor: 'gold',
                    }}
                    unselectedStyle={{
                        backgroundColor: 'silver',
                        borderTopRightRadius: 2,
                        borderBottomRightRadius: 2
                    }}
                    values={[0]}
                    max={15}
                    containerStyle={{
                        height: 35,
                    }}
                    trackStyle={{
                        height: 5,
                    }}
                    touchDimensions={{
                        height: 35,
                        width: 35,
                        borderRadius: 20,
                        slipDisplacement: 35,
                    }}
                    customMarker={CustomMarker}
                    sliderLength={this.sliderLength}
                    markerOffsetY={-6}
                    snapped={false}
                />
                <View style={styles.disabledView} />
            </View>
            <View style={[styles.silverLiner, { marginBottom: 10 }]} />

            <View style={styles.timeRow}>
                <Text style={[styles.itemTitle,
                {
                    flex: 1,
                    textAlign: "center",
                    fontSize: 14,
                }]}>
                    Ride-hailing apps average standard price is {' ₦' + this.state.trip.cost.toFixed(2)}{'\n'}
                    Destination travel time: {this.formatTime()}
                </Text>
            </View>

            <View style={styles.silverLiner} />
            <View style={styles.buttonsRow}>
                <TouchableOpacity
                    onPress={() => {
                        this.cancelTrip()
                        //this.changeStatus('canceled', true)
                        this.props.goBack()
                    }}
                    activeOpacity={0.8}
                    style={[styles.itemButton, { backgroundColor: '#fff' }]}
                >
                    <Text style={[styles.priceText, { color: '#d80027' }]}>
                        Cancel
                </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        if (this.state.inPlace) {
                            this.inPlaceAnimation()
                            this.changeStatus('in_progress')
                        }
                        else {
                            this.changeStatus('driver_arrived')
                            this.setState({
                                inPlace: true
                            })
                        }
                    }}
                    activeOpacity={0.8}
                    style={styles.itemButton}
                >
                    <Text style={[styles.priceText, { color: '#fff' }]}>
                        {this.state.inPlace ? 'let’s go' : 'I’m Here'}
                    </Text>
                </TouchableOpacity>
            </View>
        </Animated.View>)
    }

    _renderRideItem() {
        return (<Animated.View style={[styles.inPlaceItem, { bottom: this.rideItemAnim, paddingVertical: 20 }]}>
            {this._renderClientInfo()}
            <Text style={styles.itemTitle}>
                {this.state.trip.address_to}
            </Text>
            <View>
                <ProcessSlider
                    animateMarker={(coords) => {
                        let bodyData = {
                            driver_location_lat: coords.latitude,
                            driver_location_lng: coords.longitude
                        }
                        this.api.drivings('PATCH', bodyData, 0, this.state.trip._id, this.state.trip._etag)
                            .then((res) => {
                                this.getTrip()
                            })
                            .catch((error) => {
                                console.log(error);
                            })
                        this.props.animateMarker(coords)
                    }}
                    trip={this.state.trip}
                />
                <View style={styles.disabledView} />
            </View>
            <View style={[styles.silverLiner, { marginBottom: 10 }]} />

            <View style={styles.timeRow}>
                <Text style={[styles.itemTitle,
                {
                    flex: 1,
                    textAlign: "center",
                    fontSize: 14,
                }]}>
                    Ride-hailing apps average standard price is {'₦ ' + this.state.trip.cost.toFixed(2)}{'\n'}
                    Destination travel time: {this.formatTime()}
                </Text>
            </View>

            <View style={styles.silverLiner} />
            <View style={styles.buttonsRow}>
                <TouchableOpacity
                    onPress={() => {
                        this.cancelTrip()
                        //this.changeStatus('canceled', true)
                        this.props.goBack()
                    }}
                    activeOpacity={0.8}
                    style={[styles.itemButton, { backgroundColor: '#fff' }]}
                >
                    <Text style={[styles.priceText, { color: '#d80027' }]}>
                        Cancel
                </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.changeStatus('complete', true)
                        this.props.goBack()
                    }}
                    activeOpacity={0.8}
                    style={styles.itemButton}
                >
                    <Text style={[styles.priceText, { color: '#fff' }]}>
                        End ride
                </Text>
                </TouchableOpacity>
            </View>
        </Animated.View>)
    }

    render() {
        return (<View style={styles.content}>
            {this._renderInPlaceItem()}
            {this._renderRideItem()}
            {this.state.offer ? <OfferModal
                modalVisible={this.state.offerModalVisible}
                offer={this.state.offer}
                user={this.state.client}
                trip={this.state.trip}
                goBack={() => {
                    this.setState({
                        offerModalVisible: false
                    })
                }}
            /> : null}
        </View>);

    }
}

export default connect(
    ({ user }) => ({ user })
)(ItemsController)

const styles = StyleSheet.create({
    content: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0
    },
    inPlaceItem: {
        position: 'absolute',
        minHeight: 150,
        paddingHorizontal: 20,
        left: 15,
        right: 15,
        backgroundColor: '#fff',
        borderRadius: 4,
        elevation: 5,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 20,
        shadowOpacity: 1,
    },
    itemTitle: {
        // marginTop: 24,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    disabledView: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'transparent'
    },
    photoContainer: {
        height: 48,
        width: 48,
        borderRadius: 24,
        backgroundColor: 'silver'
    },
    priceText: {
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    timeRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15
    },
    silverLiner: {
        height: 1,
        backgroundColor: '#e5e3dd',
    },
    phoneContainer: {
        position: 'absolute',
        right: 0,
        backgroundColor: '#2ecc71',
        width: 36,
        height: 36,
        borderRadius: 24,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemButton: {
        marginTop: 15,
        alignSelf: 'center',
        height: 40,
        width: '40%',
        backgroundColor: '#2ecc71',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    timeTitle: {
        marginBottom: 10,
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: 'rgb(189, 195, 199)'
    },
    buttonsRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    image: {
        flex: 1,
        width: '100%',
        borderRadius: 28
    },
});
