import React, { Component } from 'react';
import {
    ToastAndroid,
    Alert,
    Dimensions
} from 'react-native'
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import CustomMarker from '../../costomMarker'
import { GoogleApi } from '../../../../networking/google.api'
import { connect } from 'react-redux'

class ProcessSlider extends Component {
    sliderLength = Dimensions.get('window').width - 70

    constructor(props) {
        super(props)
        this.state = {
            slideValue: 0,
            distance: this.props.trip.distance
        }
    }

    componentDidMount() {
        this.getDistance()
        this.interval = setInterval(() => {
            this.getDistance()
        }, 5000)
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    getDistance() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                let requestBody = {
                    location_from_lat: position.coords.latitude,
                    location_from_lng: position.coords.longitude,
                    location_to_lat: this.props.trip.location_to_lat,
                    location_to_lng: this.props.trip.location_to_lng,
                }
                GoogleApi.getDistance(requestBody).then((info) => {
                    console.log(info);
                    let distance = info.rows[0].elements[0].distance.value
                    console.log('distance');
                    console.log(distance);
                    console.log(this.state.distance);
                    if (distance > this.state.distance) {
                        this.setState({
                            slideValue: 0,
                            distance: distance,
                        })
                    }
                    else {
                        this.setState({
                            slideValue: this.props.trip.distance - distance
                        })
                    }
                    let coords = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    }
                    this.props.animateMarker(coords)
                    //ToastAndroid.show(coords.latitude+'|'+coords.longitude, ToastAndroid.SHORT);
                    // Alert.alert(
                    //     '',
                    //     coords.latitude+'|'+coords.longitude,
                    //     [
                    //         { text: 'Close', onPress: () => { }, style: 'cancel' },
                    //     ],
                    //     { cancelable: false }
                    // )
                    console.log(coords);

                    // this.props.dispatch({
                    //     type: 'SET_DRIVER_LOCATION', value: coords
                    // })
                })
                    .catch((error) => {
                        console.log(error);
                    })
            },
            (error) => {
                console.log('error');
                console.log(error);
            },
            { enableHighAccuracy: false }
        );
    }

    render() {
        return (
            <MultiSlider
                selectedStyle={{
                    borderRadius: 2,
                    backgroundColor: 'gold',
                }}
                unselectedStyle={{
                    backgroundColor: 'silver',
                    borderTopRightRadius: 2,
                    borderBottomRightRadius: 2
                }}
                values={[this.state.slideValue]}
                max={this.state.distance}
                containerStyle={{
                    height: 35,
                }}
                trackStyle={{
                    height: 5,
                }}
                touchDimensions={{
                    height: 35,
                    width: 35,
                    borderRadius: 20,
                    slipDisplacement: 35,
                }}
                customMarker={CustomMarker}
                sliderLength={this.sliderLength}
                markerOffsetY={-6}
                snapped={false}
            />
        );
    }
}

export default connect()(ProcessSlider)