import { StackNavigator } from 'react-navigation';
import DriverHistory from './DriverHistory'
import OrderInfoModal from './orderInfoModal'

export const DriverHistoryNavigator = StackNavigator({
    historyList: {
        screen: DriverHistory
    },
    orderInfoModal: {
        screen: OrderInfoModal
    }
},
    {
        headerMode: 'none',
        //initialRouteName: 'chooseCity'
    });