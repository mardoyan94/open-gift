import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    FlatList,
    TouchableNativeFeedback,
    Alert,
    Platform
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import API from '../../networking/api'
import { BarIndicator } from 'react-native-indicators';
import ItemMap from '../requestHistory/itemMap'
import { connect } from 'react-redux'
import LocationSwitch from 'react-native-location-switch';
import RNIap, {
    Product,
    ProductPurchase,
    acknowledgePurchaseAndroid,
    purchaseUpdatedListener,
    purchaseErrorListener,
    PurchaseError,
} from 'react-native-iap';
import SubscibeModal from './buySubscibeModal'

const itemSkus = Platform.select({
    ios: [
        'com.example.coins100'
    ],
    android: [
        'weekly', 'monthly'
    ]
});

class NearbyDrivings extends Component {
    coords = {}
    weekdays = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ]
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    page = 1
    api = new API()
    purchases = []
    constructor(props) {
        super(props)
        this.state = {
            items: [],
            listLoading: true,
            empty: false,
            productList: [],
            subscribeModalVisible: false
        }
    }

    async getItems(i) {
        try {
            const subscriptions = await RNIap.getSubscriptions(itemSkus);
            console.log(subscriptions);
            await this.buySubscribeItem(subscriptions[i].productId)
        } catch (err) {
            console.warn(err); // standardized err.code and err.message available
        }
    }

    async buySubscribeItem(sku) {
        try {
            console.log('buySubscribeItem: ' + sku);
            const purchase = await RNIap.requestSubscription(sku);
            // console.info(purchase);
            // Alert.alert(purchase);
        } catch (err) {
            console.warn(err.code, err.message);
            Alert.alert(err.message);
        }
    }

    async getPurchases() {
        try {
            const purchases = await RNIap.getAvailablePurchases();
            console.log(purchases);
            return purchases
        } catch (err) {
            console.warn(err); // standardized err.code and err.message available
            Alert.alert(err.message);
        }
    }

    buySubscribe(i) {
        this.getItems(i).then(() => {
            this.getPurchases().then((purchases) => {
                if (purchases && purchases.length > 0) {
                    this.purchases = purchases
                    Alert.alert(
                        '',
                        'You have successfully purchased a subscription.',
                        [
                            { text: 'Close', onPress: () => { }, style: 'cancel' },
                        ],
                        { cancelable: false }
                    )
                }
            })
                .catch((error) => {
                    console.log(error);
                })
        })
            .catch((error) => {
                console.log(error);

            })
    }

    componentDidMount() {
        this.getPurchases().then((purchases) => {
            this.purchases = purchases
            this.getTripList()
        })
            .catch((error) => {
                console.log(error);
            })
    }

    getTripList() {
        LocationSwitch.isLocationEnabled(
            () => {
                navigator.geolocation.getCurrentPosition(
                    (position) => {
                        let coordinates = {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                        }
                        this.coords = coordinates
                        console.log('$$$$$$$$$', this.coords);

                        this.startGeting()
                    },
                    (error) => {
                        this.startGeting()
                    },
                    { enableHighAccuracy: false }
                );
            },
            () => {
                this.startGeting()
            },
        );
    }

    startGeting() {
        if (this.coords.latitude) {
            this.getPageByLocation()
            this.interval = setInterval(() => {
                this.getStandbleListByLocation()
            }, 10000)
        }
        else {

            this.getPage()
            this.interval = setInterval(() => {
                this.getStandbleList()
            }, 10000)
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    getStandbleListByLocation() {
        this.setState({
            items: [],
            listLoading: true,
            empty: false
        })
        this.page = 1
        bodyData = {
            status: 'new',
        }
        this.api.getDrivingsByLocation(bodyData, this.page, this.coords, this.props.user.info._id).then((res) => {
            console.log(res);
            console.log('dddddddddddddddddddddddddddddddddddddd');
            if (res._items.length == 0) {
                this.setState({
                    empty: true,
                    listLoading: false
                })
            }
            else if (res._links.next) {
                this.setState({
                    items: res._items,
                })
            }
            else {
                this.setState({
                    items: res._items,
                    listLoading: false
                })
            }
        })
            .catch((error) => {
                console.log(error);
                this.setState({
                    listLoading: false
                })
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    getStandbleList() {
        this.setState({
            items: [],
            listLoading: true,
            empty: false
        })
        this.page = 1
        bodyData = {
            status: 'new',
        }
        this.api.drivings('GET', bodyData, this.page).then((res) => {
            console.log(res);
            console.log('dddddddddddddddddddddddddddddddddddddd');
            if (res._items.length == 0) {
                this.setState({
                    empty: true,
                    listLoading: false
                })
            }
            else if (res._links.next) {
                this.setState({
                    items: res._items,
                })
            }
            else {
                this.setState({
                    items: res._items,
                    listLoading: false
                })
            }
        })
            .catch((error) => {
                console.log(error);
                this.setState({
                    listLoading: false
                })
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    getPageByLocation() {
        bodyData = {
            status: 'new',
        }
        this.api.getDrivingsByLocation(bodyData, this.page, this.coords, this.props.user.info._id).then((res) => {
            console.log(res);
            this.page++
            console.log('dddddddddddddddddddddddddddddddddddddd');
            if (res._items.length == 0) {
                this.setState({
                    empty: true,
                    listLoading: false
                })
            }
            else if (res._links.next) {
                this.setState({
                    items: [...this.state.items, ...res._items]
                })
            }
            else {
                this.setState({
                    items: [...this.state.items, ...res._items],
                    listLoading: false
                })
            }
        })
            .catch((error) => {
                console.log(error);
                this.setState({
                    listLoading: false
                })
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    getPage() {
        bodyData = {
            status: 'new',
        }
        this.api.drivings('GET', bodyData, this.page).then((res) => {

            this.page++
            console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
            console.log(res);
            if (res._items.length == 0) {
                this.setState({
                    empty: true,
                    listLoading: false
                })
            }
            else if (res._links.next) {
                this.setState({
                    items: [...this.state.items, ...res._items]
                })
            }
            else {
                this.setState({
                    items: [...this.state.items, ...res._items],
                    listLoading: false
                })
            }
        })
            .catch((error) => {
                console.log(error);
                this.setState({
                    listLoading: false
                })
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    _renderEmpty() {
        return this.state.empty ?
            <Text style={styles.emptyText}>
                Is Empty
        </Text> : null
    }

    renderFooter = () => {
        if (!this.state.listLoading) return null;
        return (
            <View
                style={{
                    width: '100%',
                    alignItems: 'center',
                    marginVertical: 16
                }}
            >
                <BarIndicator
                    size={40}
                    count={7}
                    color='#fff'
                />
            </View>
        );
    };

    deleteTrip(trip) {
        this.api.drivings('DELETE', null, null, trip._id, trip._etag).then((res) => {
            console.log('deleted');

        })
            .catch((error) => {
                console.log(error);

            })
    }

    acceptTrip(trip) {
        console.log(trip._id);

        let bodyData = {
            status: 'driver_found',
            driver: this.props.user.info._id
        }
        this.api.drivings('PATCH', bodyData, 0, trip._id, trip._etag)
            .then((res) => {
                console.log('PATCH-driver_found');
                console.log(res);
                if (res) {
                    this.api.drivings('GET', null, null, trip._id).then((res) => {
                        console.log('|||||||||||||||||||||||||||||||||||||||||||||');

                        console.log(res);
                        this.props.navigation.navigate('orderMap', res)
                    })
                        .catch((error) => {
                            console.log(error);

                        })
                }
                else {
                    Alert.alert(
                        '',
                        'Driver already found for this driving',
                        [
                            { text: 'Close', onPress: () => { }, style: 'cancel' },
                        ],
                        { cancelable: false }
                    )
                }


            })
            .catch((error) => {
                console.log(error);
            })
    }

    _renderList() {
        return (<FlatList
            contentContainerStyle={{ paddingVertical: 10 }}
            showsVerticalScrollIndicator={false}
            initialNumToRender={25}
            //maxToRenderPerBatch={1}
            onEndReachedThreshold={0.1}
            keyExtractor={(item, index) => index.toString()}
            scrollToEnd={() => { console.log('end list') }}
            style={{ width: '100%' }}
            data={this.state.items}
            renderItem={({ item }) => {
                let date = new Date(item._created)
                let formatDate = this.weekdays[date.getDay()] + ', ' + date.getDate() + ' ' + this.months[date.getMonth() - 1]
                let fromCoords = {
                    lat: item.location_from_lat,
                    lng: item.location_from_lng
                }
                let toCoords = {
                    lat: item.location_to_lat,
                    lng: item.location_to_lng
                }

                return (<TouchableNativeFeedback
                    onPress={() => {
                        if (this.purchases.length > 0) {
                            this.props.navigation.navigate('infoModal', {
                                ...item,
                                formatDate: formatDate
                            })
                        }
                        else {
                            this.setState({
                                subscribeModalVisible: true
                            })
                        }
                    }}
                >
                    <View style={styles.itemContainer}>
                        <View style={styles.itemMapContainer}>
                            <ItemMap
                                fromCoords={fromCoords}
                                toCoords={toCoords}
                            />
                        </View>
                        <View style={{
                            marginTop: 20,
                            marginHorizontal: 20
                        }}>
                            <View style={styles.itemRowPrice}>
                                <View style={styles.itemIconContainer}>
                                    <Image
                                        style={{ width: 32, height: 32 }}
                                        source={require('../../assets/icons/clock.png')}
                                    />
                                    <Text style={styles.priceText}>
                                        {Math.round(item.duration / 60)} minutes
                            </Text>
                                </View>
                                <View style={styles.innerLine} />
                                <View style={styles.itemIconContainer}>
                                    <Image
                                        style={{ width: 27, height: 32 }}
                                        source={require('../../assets/icons/moneyBag.png')}
                                    />
                                    <Text style={styles.priceText}>
                                        ₦{item.cost == 0 ? ' - ' : item.cost.toFixed(2)}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.priceLine} />
                            <View style={[styles.pointRow, { marginVertical: 18 }]}>
                                <Image
                                    style={{
                                        marginRight: 8.7,
                                        width: 10.3,
                                        height: 15.5
                                    }}
                                    source={require('../../assets/icons/marker.png')}
                                />
                                <Text style={[styles.priceText, { marginRight: 20, }]}>
                                    {item.address_from}
                                </Text>
                            </View>
                            <View style={styles.pointLine} />
                        </View>
                        <View style={styles.buttonsRow}>
                            <View />
                            <TouchableOpacity
                                onPress={() => {
                                    if (this.purchases.length > 0) {
                                        this.acceptTrip(item)
                                    }
                                    else {
                                        this.setState({
                                            subscribeModalVisible: true
                                        })
                                    }

                                }}
                                style={styles.button}>
                                <Text style={[styles.ButtonText, { color: '#2ecc71' }]}>
                                    Accept
                                    </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableNativeFeedback>)
            }}
            ListFooterComponent={this.renderFooter}
            onEndReached={() => {
                if (this.state.listLoading) {
                    if (this.coords.latitude) {
                        this.getPageByLocation()
                    }
                    else {
                        this.getPage()
                    }

                }
            }
            }
        />)
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.dispatch(NavigationActions.back())
                        }}
                        activeOpacity={0.8}
                        style={styles.backIconContainer}>
                        <Image
                            style={{ width: 11, height: 20 }}
                            source={require('../../assets/icons/back.png')}
                        />
                        <Text style={styles.backText}>
                            Back
                        </Text>
                    </TouchableOpacity>
                    <Text style={styles.headerText}>
                        My Trip
                    </Text>
                </View>
                <View style={styles.content}>
                    {this._renderList()}
                    {this._renderEmpty()}
                </View>
                <SubscibeModal
                    modalVisible={this.state.subscribeModalVisible}
                    close={() => {
                        this.setState({
                            subscribeModalVisible: false
                        })
                    }}
                    buy={(i) => {
                        this.setState({
                            subscribeModalVisible: false
                        })
                        this.buySubscribe(i)
                    }}
                />
            </View>
        );
    }
}

export default connect(
    ({ user }) => ({ user })
)(NearbyDrivings)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        backgroundColor: '#fff',
        height: 38,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backIconContainer: {
        position: 'absolute',
        left: 18,
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'Roboto-Bold',
        color: 'rgb(44, 62, 80)',
    },
    backText: {
        marginLeft: 10,
        color: '#2ecc71',
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    },
    content: {
        flex: 1,
        backgroundColor: '#2ecc71',
    },
    itemContainer: {
        marginHorizontal: 15,
        marginVertical: 10,
        backgroundColor: '#fff',
        minHeight: 285,
        borderRadius: 4,
        elevation: 15,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 20,
        shadowOpacity: 1,
        borderRadius: 4
    },
    itemMapContainer: {
        height: 129,
        backgroundColor: '#ece9e1'
    },
    itemRowPrice: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 57,
        marginBottom: 10
    },
    priceText: {
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    priceLine: {
        backgroundColor: '#2ecc71',
        height: 2,
        marginTop: 8
    },
    pointRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    timeRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    timeLine: {
        height: 14,
        width: 1,
        backgroundColor: 'rgb(189, 195, 199)',
        marginLeft: 5,
        marginTop: 4,
        marginBottom: 7
    },
    timeText: {
        marginLeft: 13,
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: 'rgb(189, 195, 199)'
    },
    map: {
        flex: 1,
    },
    emptyText: {
        position: 'absolute',
        top: 200,
        alignSelf: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 30,
        color: '#fff'
    },
    itemIconContainer: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    innerLine: {
        height: 57,
        width: 1,
        backgroundColor: '#e5e3dd'
    },
    pointLine: {
        backgroundColor: '#e5e3dd',
        height: 1
    },
    buttonsRow: {
        height: 56,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        paddingHorizontal: 20,
        justifyContent: 'center',
    },
    ButtonText: {
        fontSize: 16,
        lineHeight: 19,
        fontFamily: 'Roboto-Regular',
    }
});
