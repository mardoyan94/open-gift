import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    Alert
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import API from '../../networking/api'
import ItemMap from '../requestHistory/itemMap'
import { Rating, AirbnbRating } from 'react-native-ratings';
import { connect } from 'react-redux'

class InfoModal extends Component {
    alphabet = ["b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    api = new API()
    constructor(props) {
        super(props)
        this.state = {
            prices: null,
            price: ' --',
            client: null,
            minutes: [2, 5, 10, 15],
            min: 5
        }
        const { params } = this.props.navigation.state
        let price = params.cost
        this.state.prices = [
            (price + (10 * price / 100)).toFixed(2),
            (price + (20 * price / 100)).toFixed(2),
            (price + (30 * price / 100)).toFixed(2),
        ]
    }

    componentDidMount() {
        this.getUser()
    }

    getUser() {
        const { params } = this.props.navigation.state
        this.api.getUserById(params.customer).then((res) => {
            console.log(res);
            this.setState({
                client: res
            })
        })
            .catch((error) => {
                console.log(error);

            })
    }

    acceptTrip(trip) {
        const { params } = this.props.navigation.state
        console.log(trip._id);
        if (this.state.price != ' --') {
            let bodyData = {
                driver: this.props.user.info._id,
                amount: this.state.price,
                driving: params._id,
                confirmed_by_customer: false,
                confirmed_by_driver: true
            }
            console.log(bodyData);

            this.api.createDrivingsOffer(bodyData).then((res) => {
                console.log(res);
                Alert.alert(
                    '',
                    'Your request has been sent!',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
                .catch((error) => {
                    console.log(error);

                })
        }
        else {
            let bodyData = {
                status: 'driver_found',
                driver: this.props.user.info._id,
                eta_driver_time: this.state.min
            }
            this.api.drivings('PATCH', bodyData, 0, trip._id, trip._etag)
                .then((res) => {
                    console.log('PATCH-driver_found');
                    console.log(res);
                    this.api.drivings('GET', null, null, trip._id).then((res) => {

                        console.log(res);
                        this.props.navigation.navigate('orderMap', res)
                    })
                        .catch((error) => {
                            console.log(error);

                        })
                })
                .catch((error) => {
                    console.log(error);
                })
        }
    }

    changeTime(i) {
        this.setState({
            min: this.state.minutes[i]
        })

    }

    changePrice(i) {
        if (this.state.price == this.state.prices[i]) {
            this.setState({
                price: ' --'
            })
        }
        else {
            this.setState({
                price: this.state.prices[i]
            })
        }
    }

    renderClientInfo() {
        return this.state.client == null ? null :
            (<View><View style={[styles.timeRow, { marginTop: 16 }]}>
                <View style={styles.photoContainer}>
                    <Image
                        style={styles.image}
                        source={this.state.client.avatar ?
                            { uri: 'data:image/png;base64,' + this.state.client.avatar } :
                            require('../../assets/images/1.png')}
                    />
                </View>
                <View style={{
                    marginLeft: 15
                }}>
                    <View style={{
                        width: 83
                    }}>
                        <AirbnbRating
                            defaultRating={4}
                            size={11}
                            isDisabled={true}
                            showRating={false} />
                    </View>
                    <Text style={styles.priceText}>
                        {this.state.client.firstname + ' ' + this.state.client.lastname}
                    </Text>
                </View>
            </View>
                <View style={styles.silverLiner} />
            </View>)
    }

    _renderPrices() {
        return (<View>
            <Text style={styles.rowText}>
                Current Price
    </Text>
            <Text style={[styles.priceText,
            {
                marginTop: 5,
                color: '#2ecc71'
            }]}>
                ₦{this.state.price}
            </Text>

            <Text style={[styles.rowText, {
                marginTop: 20,
            }]}>
                Offer your Price
    </Text>

            <View style={styles.textsRow}>
                {this.state.prices.map((item, i) => (<TouchableOpacity
                    key={i}
                    onPress={() => {
                        this.changePrice(i)
                    }}
                    activeOpacity={0.8}
                    style={[styles.offerPriceItem,
                    this.state.price == item ?
                        { backgroundColor: '#2ecc71' } :
                        { backgroundColor: '#ecf0f1' }
                    ]}
                >
                    <Text style={[styles.priceText,
                    this.state.price == item ?
                        { color: '#fff' } :
                        null
                    ]}>
                        ₦{item}
                    </Text>
                </TouchableOpacity>))}
            </View>
        </View>)
    }

    _renderMinutes() {
        return (<View style={{ marginVertical: 20 }}>
            <Text style={styles.rowText}>
            When are you arriving?
    </Text>
            <View style={[styles.textsRow,
            {
                marginBottom: 15
            }]}>
                {this.state.minutes.map((item, i) => (<TouchableOpacity
                    key={i}
                    onPress={() => {
                        this.changeTime(i)
                    }}
                    activeOpacity={0.8}
                    style={[styles.offerPriceItem,
                    this.state.min == item ?
                        { backgroundColor: '#2ecc71' } :
                        { backgroundColor: '#ecf0f1' }
                    ]}
                >
                    <Text style={[styles.priceText,
                    this.state.min == item ?
                        { color: '#fff' } :
                        null
                    ]}>
                        {item}min
                        </Text>
                </TouchableOpacity>))}
            </View>
        </View>)
    }

    _renderToPoints(params) {
        let points = JSON.parse(params.destinations)
        return points.map((item, i) => (<View
            key={i}
        >
            <View style={[styles.pointRow]}>
                <View style={styles.pointIcon}>
                    <Text style={styles.pointIconText}>
                        {this.alphabet[i].toLocaleUpperCase()}
                    </Text>
                </View>
                <Text
                    numberOfLines={1}
                    style={styles.priceText}>
                    {item.address}
                </Text>
            </View>
            {i == points.length - 1 ? null
                : <View style={styles.timeRow}>
                    <View style={styles.timeLine} />
                    {/* <Text style={styles.timeText}>
                    19 minutes        15.20 — 16.24
        </Text> */}
                </View>}
        </View>))
    }

    render() {
        const { params } = this.props.navigation.state
        let fromCoords = {
            lat: params.location_from_lat,
            lng: params.location_from_lng
        }
        let toCoords = {
            lat: params.location_to_lat,
            lng: params.location_to_lng
        }
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.dispatch(NavigationActions.back())
                        }}
                        activeOpacity={0.8}
                        style={styles.backIconContainer}>
                        <Image
                            style={{ width: 11, height: 20 }}
                            source={require('../../assets/icons/back.png')}
                        />
                        <Text style={styles.backText}>
                            Back
                        </Text>
                    </TouchableOpacity>
                    <Text style={styles.headerText}>
                        My Trip
                    </Text>
                </View>
                <View style={styles.content}>
                    <View style={styles.itemContainer}>
                        <View style={styles.itemMapContainer}>
                            <ItemMap
                                fromCoords={fromCoords}
                                toCoords={toCoords}
                            />
                        </View>
                        <ScrollView style={{
                            marginBottom: 50
                        }}>
                            <View style={{ marginHorizontal: 20, marginTop: 20 }}>
                                <View style={styles.itemRowPrice}>
                                    <View style={styles.itemIconContainer}>
                                        <Image
                                            style={{ width: 32, height: 32 }}
                                            source={require('../../assets/icons/clock.png')}
                                        />
                                        <Text style={styles.priceText}>
                                            {Math.round(params.duration/60)} minutes
                                </Text>
                                    </View>
                                    <View style={styles.innerLine} />
                                    <View style={styles.itemIconContainer}>
                                        <Image
                                            style={{ width: 27, height: 32 }}
                                            source={require('../../assets/icons/moneyBag.png')}
                                        />
                                        <Text style={styles.priceText}>
                                            ₦{params.cost == 0 ? ' - ' : params.cost.toFixed(2)}
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.priceLine} />
                                <View style={[styles.pointRow, { marginTop: 18 }]}>
                                    <View style={styles.pointIcon}>
                                        <Text style={styles.pointIconText}>
                                            A
                                        </Text>
                                    </View>
                                    <Text
                                        numberOfLines={1}
                                        style={[styles.priceText, {flex: 1}]}>
                                        {params.address_from}
                                    </Text>
                                </View>
                                <View style={styles.timeRow}>
                                    <View style={styles.timeLine} />
                                    {/* <Text style={styles.timeText}>
                                        19 minutes        15.20 — 16.24
                        </Text> */}
                                </View>
                                {this._renderToPoints(params)}
                                <View style={styles.silverLiner} />
                                {this.renderClientInfo()}
                                {this._renderPrices()}
                                {this._renderMinutes()}
                            </View>
                        </ScrollView>
                        <View style={styles.buttonsContainer}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.goBack()
                                }}
                                style={styles.button}
                            >
                                <Text style={[styles.ButtonText, { color: 'rgb(189, 195, 199)' }]}>
                                    Skip
                                        </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.acceptTrip(params)
                                }}
                                style={styles.button}
                            >
                                <Text style={[styles.ButtonText, { color: '#2ecc71' }]}>
                                    Accept
                                        </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

export default connect(
    ({ user }) => ({ user })
)(InfoModal)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        backgroundColor: '#fff',
        height: 38,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backIconContainer: {
        position: 'absolute',
        left: 18,
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'Roboto-Bold',
        color: 'rgb(44, 62, 80)',
    },
    backText: {
        marginLeft: 10,
        color: '#2ecc71',
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    },
    content: {
        flex: 1,
        backgroundColor: '#2ecc71',
    },
    itemContainer: {
        flex: 1,
        marginHorizontal: 15,
        marginVertical: 20,
        backgroundColor: '#fff',
        borderRadius: 4,
        elevation: 15,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 20,
        shadowOpacity: 1,
        borderRadius: 4
    },
    itemMapContainer: {
        height: 129,
        backgroundColor: '#ece9e1'
    },
    itemRowPrice: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 57,
        marginBottom: 10
    },
    priceText: {
        //marginLeft: 10,
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: 'rgb(44, 62, 80)'
    },
    priceLine: {
        backgroundColor: '#2ecc71',
        height: 2,
        marginTop: 8
    },
    pointRow: {
        flexDirection: 'row',
    },
    timeRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    timeLine: {
        height: 14,
        width: 1,
        backgroundColor: 'rgb(189, 195, 199)',
        marginLeft: 9,
        marginTop: 4,
        marginBottom: 7
    },
    timeText: {
        marginLeft: 13,
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: 'rgb(189, 195, 199)'
    },
    rowText: {
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: 'rgb(44, 62, 80)'
    },
    silverLiner: {
        height: 1,
        backgroundColor: '#e5e3dd',
        marginTop: 16
    },

    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        height: 60,
        position: 'absolute',
        bottom: 0,
    },
    button: {
        paddingHorizontal: 20,
        height: '100%',
        justifyContent: 'center',
    },
    ButtonText: {
        fontSize: 16,
        lineHeight: 19,
        fontFamily: 'Roboto-Regular',
    },
    itemIconContainer: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    innerLine: {
        height: 57,
        width: 1,
        backgroundColor: '#e5e3dd'
    },
    pointLine: {
        backgroundColor: '#e5e3dd',
        height: 1
    },
    photoContainer: {
        height: 48,
        width: 48,
        borderRadius: 24,
        backgroundColor: 'silver'
    },
    textsRow: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    offerPriceItem: {
        marginTop: 10,
        marginRight: 10,
        paddingHorizontal: 5,
        height: 30,
        minWidth: 53,
        //backgroundColor: '#2ecc71',
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    currentPriceItem: {
        height: 30,
        justifyContent: 'center',
    },
    image: {
        flex: 1,
        width: '100%',
        borderRadius: 28
    },
    pointIconText: {
        fontSize: 12,
        color: '#fff',
        fontFamily: 'Roboto-Regular'
    },
    pointIcon: {
        marginRight: 10,
        height: 18,
        width: 18,
        borderRadius: 10,
        backgroundColor: '#2ecc71',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
