import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Keyboard,
    Image,
    Modal,
    Alert,
    Dimensions
} from 'react-native';

export default class SubscibeModal extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.modalVisible}
                onRequestClose={() => { }}>
                <View
                    style={styles.container}>
                    <View style={styles.alertContainer}>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.close()
                            }}
                            style={styles.closeButton}>
                            <Image
                                style={{
                                    width: 25,
                                    height: 25
                                }}
                                source={require('../../assets/icons/close.png')}
                            />
                        </TouchableOpacity>
                        <Text style={styles.descText}>
                            In order to receive Ride offers, you need to Subscribe
                        </Text>
                        <View style={styles.buttonsRow}>
                            <TouchableOpacity
                            onPress={()=>{
                                this.props.buy(0)
                            }}
                            style={[styles.button, { backgroundColor: '#2ecc71' }]}>
                                <Text style={[styles.buttonText, { color: '#fff' }]}>Month</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>{
                                    this.props.buy(1)
                                }}
                            style={styles.button}>
                            <Text style={styles.buttonText}>Week</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        alignItems: "center",
        justifyContent: "center"
    },
    alertContainer: {
        alignItems: "center",
        borderRadius: 7,
        backgroundColor: '#fff',
        width: Dimensions.get("window").width - 30
    },
    closeButton: {
        alignSelf: "flex-end",
        margin: 15
    },
    descText: {
        textAlign: "center",
        fontFamily: 'Roboto-Regular',
        fontSize: 20,
        color: 'rgb(44, 62, 80)',
        marginHorizontal: 50
    },
    buttonsRow: {
        flexDirection: 'row',
        marginTop: 25,
        marginBottom: 30,
        height: 50,
        width: Dimensions.get("window").width - 60,
        borderColor: '#2ecc71',
        borderWidth: 3,
        borderRadius: 7
    },
    button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText:{
        fontFamily: 'Roboto-Regular',
        fontSize: 18,
        color: 'rgb(44, 62, 80)',
    }
});
