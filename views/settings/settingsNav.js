import { StackNavigator } from 'react-navigation';
import Settings from './settings'

export const SettingsNav = StackNavigator({
    settings: {
        screen: Settings
    }
},
    {
        headerMode: 'none',
        //initialRouteName: 'chooseCity'
    });