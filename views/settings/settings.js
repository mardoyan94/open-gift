import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    ImageBackground,
    ScrollView,
    Alert
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import API from '../../networking/api'
import { BarIndicator } from 'react-native-indicators';
import ImagePicker from 'react-native-image-picker';
//import { NavigationActions } from 'react-navigation'

// const resetAction = NavigationActions.reset({
//     index: 0,
//     actions: [
//       NavigationActions.navigate({ routeName: 'carBrand'}),
//     ]
//   })
//   this.props.navigation.dispatch(resetAction)

class Settings extends Component {
    api = new API()

    constructor(props) {
        super(props)
        this.state = {
            userInfo: {
                firstname: this.props.user.info.firstname,
                lastname: this.props.user.info.lastname,
                email: this.props.user.info.email,
                driver_licence_number: this.props.user.info.driver_licence_number ? this.props.user.info.driver_licence_number : '',
                driver_licence_end_date: this.props.user.info.driver_licence_end_date ? this.props.user.info.driver_licence_end_date : '',
                driver_ownership_doc_number: this.props.user.info.driver_ownership_doc_number ? this.props.user.info.driver_ownership_doc_number : '',
                driver_car_licence_plate_number: this.props.user.info.driver_car_licence_plate_number ? this.props.user.info.driver_car_licence_plate_number : '',
                driver_car_model: null,
                driver_car_color: null
            },
            loading: false
        }
    }

    componentDidMount() {
        if (this.props.user.info.driver_car_model) {
            this.api.getCarModalById(this.props.user.info.driver_car_model).then((res) => {
                console.log('getCarModalById');
                console.log(res);
                let data = this.state.userInfo
                data.driver_car_model = res
                this.setState({
                    userInfo: data
                })

            })
                .catch((error) => {
                    console.log(error);

                })
            this.api.getColorById(this.props.user.info.driver_car_color).then((res) => {
                console.log('getColorById');
                console.log(res);
                let data = this.state.userInfo
                data.driver_car_color = res
                this.setState({
                    userInfo: data
                })
            })
                .catch((error) => {
                    console.log(error);

                })
        }
    }

    uploadImage() {
        const options = {
            title: 'Select Avatar',
            //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = response.uri

                // You can also display the image using data:
                //const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    loading: true
                })
                this.api.uploadFile('avatar', source, this.props.user.info).then((res) => {
                    console.log(res);
                    this.getUser()
                })
                    .catch((error) => {
                        console.log(error);

                    })
            }
        });
    }

    _renderLoading() {
        return this.state.loading ?
            (<View style={styles.loadingContainer}>
                <BarIndicator
                    count={7}
                    color='#2ecc71' />
            </View>) :
            null
    }

    saveSettings() {
        Keyboard.dismiss()
        let bodyData = {}

        let object = { ...this.state.userInfo }
        Object.keys(object).map(function (objectKey, index) {
            var value = object[objectKey];
            console.log(value);

            if (value && value.length > 0) {
                bodyData[objectKey] = value
            }
        });

        this.setState({
            loading: true
        })

        this.api.changeUserInfo(bodyData, this.props.user.info).then((res) => {
            console.log(res);
            if (res == 200) {
                this.getUser()
            }
            else {
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            }

        })
            .catch((error) => {
                console.log(error);

            })
    }

    getUser() {
        this.api.users('GET', {}, this.props.user.info.phone).then((res) => {
            console.log(res);
            this.props.dispatch({ type: 'SET_USER', value: res._items[0] })
            this.setState({
                loading: false
            })
            Alert.alert(
                '',
                'Your settings are saved',
                [
                    { text: 'Close', onPress: () => { }, style: 'cancel' },
                ],
                { cancelable: false }
            )
        })
            .catch((error) => {
                console.log(error);
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    _renderSmallInput(key, placeholder) {
        return (<View style={[
            styles.inputContainer,
            this.state.userInfo[key] == '' ?
                { borderBottomColor: 'rgb(189, 195, 199)' } :
                null]}>
            <TextInput
                onChangeText={(text) => {
                    let obj = {}
                    obj[key] = text
                    this.setState({
                        userInfo: { ...this.state.userInfo, ...obj }
                    })
                }}
                style={styles.smallInput}
                value={this.state.userInfo[key]}
                underlineColorAndroid="transparent"
                placeholder={placeholder}
            />
        </View>)
    }

    _renderInfoButtons(key, placeholder) {
        return (<TouchableOpacity
            onPress={() => {
                this.props.navigation.navigate('driverModNav')
            }}
            activeOpacity={0.8}
            style={[
                styles.inputContainer,
                this.state.userInfo[key] == '' ?
                    { borderBottomColor: 'rgb(189, 195, 199)' } :
                    null]}>
            <Image
                style={{
                    width: 15.7,
                    height: 20
                }}
                source={require('../../assets/icons/document.png')}
            />
            <Text
                style={[styles.input, { textAlignVertical: "center" },
                this.state.userInfo[key] == '' ? { color: 'rgb(189, 195, 199)' } : null
                ]}
            >{this.state.userInfo[key] == '' ?
                placeholder :
                this.state.userInfo[key]
                }</Text>
        </TouchableOpacity>)
    }

    _renderDriverInfo() {
        return this.props.user.info.driver_car_model ? (<View>
            <TouchableOpacity
                onPress={() => {
                    this.props.navigation.navigate('driverModNav')
                }}
                activeOpacity={0.8}
                style={[
                    styles.inputContainer,
                    this.state.userInfo.driver_car_model == null ?
                        { borderBottomColor: 'rgb(189, 195, 199)' } :
                        null]}>
                <Image
                    style={{
                        width: 20,
                        height: 15.7
                    }}
                    source={require('../../assets/icons/taxi.png')}
                />
                <Text
                    style={[styles.input, { textAlignVertical: "center" }]}
                >{this.state.userInfo.driver_car_model == null ?
                    'Please wait...' :
                    this.state.userInfo.driver_car_model.title
                    }</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => {
                    this.props.navigation.navigate('driverModNav')
                }}
                activeOpacity={0.8}
                style={[
                    styles.inputContainer,
                    this.state.userInfo.driver_car_color == null ?
                        { borderBottomColor: 'rgb(189, 195, 199)' } :
                        null]}>
                <Image
                    style={{
                        width: 20,
                        height: 15.7
                    }}
                    source={require('../../assets/icons/artist.png')}
                />
                {this.state.userInfo.driver_car_color == null ? null :
                    <View style={[styles.colorDot, { backgroundColor: this.state.userInfo.driver_car_color.code }]} />
                }
                <Text
                    style={[styles.input, { textAlignVertical: "center" }]}
                >{this.state.userInfo.driver_car_color == null ?
                    'Please wait...' :
                    this.state.userInfo.driver_car_color.title
                    }</Text>
            </TouchableOpacity>
            {this._renderInfoButtons('driver_licence_number', 'Driver licence number')}
            {this._renderInfoButtons('driver_licence_end_date', 'Driver licence end date')}
            {this._renderInfoButtons('driver_ownership_doc_number', 'Driver ownership doc number')}
            {this._renderInfoButtons('driver_car_licence_plate_number', 'Driver car licence plate number')}
        </View>) : null
    }

    _renderInput(key, placeholder) {
        return (<View style={[
            styles.inputContainer,
            this.state.userInfo[key] == '' ?
                { borderBottomColor: 'rgb(189, 195, 199)' } :
                null]}>
            <Image
                style={{
                    width: 20.4,
                    height: 14.3
                }}
                source={require('../../assets/icons/mail.png')}
            />
            <TextInput
                onChangeText={(text) => {
                    let obj = {}
                    obj[key] = text
                    this.setState({
                        userInfo: { ...this.state.userInfo, ...obj }
                    })
                }}
                style={styles.input}
                value={this.state.userInfo[key]}
                underlineColorAndroid="transparent"
                placeholder={placeholder}
            />
        </View>)

    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress={() => {
                    Keyboard.dismiss()
                }}
            >
                <ImageBackground
                    source={require('../../assets/images/background.png')}
                    style={styles.container}>
                    <View style={styles.header}>
                        <TouchableOpacity
                            onPress={() => {
                                Keyboard.dismiss()
                                this.props.navigation.dispatch(NavigationActions.back())
                            }}
                            activeOpacity={0.8}
                            style={styles.iconContainer}>
                            <Image
                                style={{ width: 11, height: 20 }}
                                source={require('../../assets/icons/back.png')}
                            />
                            <Text style={styles.backText}>
                                Back
                        </Text>
                        </TouchableOpacity>
                        <Text style={styles.headerText}>
                            Settings
                    </Text>
                        <TouchableOpacity
                            onPress={() => {
                                this.saveSettings()
                            }}
                            activeOpacity={0.8}
                            style={styles.saveButton}>
                            <Text style={styles.saveText}>
                                Save
                        </Text>
                        </TouchableOpacity>
                    </View>
                    <ScrollView>
                        <View style={styles.content}>
                            <View
                                style={styles.photoRow}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.uploadImage()
                                    }}
                                    style={styles.photoContainer}>
                                    <Image
                                        style={styles.image}
                                        source={this.props.user.info.avatar ?
                                            { uri: 'data:image/png;base64,' + this.props.user.info.avatar } :
                                            require('../../assets/images/1.png')
                                        }
                                    />
                                </TouchableOpacity>
                                <View style={styles.nameInputContainer}>
                                    {this._renderSmallInput('firstname', 'Firstname')}
                                    {this._renderSmallInput('lastname', 'Lastname')}
                                </View>
                            </View>
                            {this._renderInput('email', 'Email')}
                            {this._renderDriverInfo()}
                        </View>
                    </ScrollView>
                    {this._renderLoading()}
                </ImageBackground>
            </TouchableWithoutFeedback>
        );
    }
}

export default connect(
    ({ user }) => ({ user })
)(Settings)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        height: 38,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    iconContainer: {
        position: 'absolute',
        left: 18,
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'Roboto-Bold',
        color: 'rgb(44, 62, 80)',
    },
    backText: {
        marginLeft: 10,
        color: '#2ecc71',
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    },
    saveButton: {
        height: '80%',
        paddingHorizontal: 5,
        position: 'absolute',
        right: 15,
        //alignItems: 'center',
        justifyContent: 'center',
    },
    saveText: {
        color: '#2ecc71',
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    },
    content: {
        marginHorizontal: 20
    },
    photoRow: {
        flexDirection: 'row',
        marginTop: 21
    },
    photoContainer: {
        marginTop: 10,
        height: 80,
        width: 80,
        borderRadius: 40,
        backgroundColor: 'silver'
    },
    nameInputContainer: {
        flex: 1,
        marginLeft: 15
    },
    inputContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomColor: '#2ecc71',
        borderBottomWidth: 2
    },
    smallInput: {
        flex: 1,
        height: 40,
    },
    image: {
        flex: 1,
        width: '100%',
        borderRadius: 40
    },
    input: {
        flex: 1,
        height: 48,
        marginLeft: 5,
        color: 'rgb(44, 62, 80)'
    },

    loadingContainer: {
        zIndex: 2,
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.2)',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    colorDot: {
        width: 24,
        height: 24,
        borderRadius: 12,
        marginLeft: 5
    }
});
