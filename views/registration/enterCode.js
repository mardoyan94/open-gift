import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableWithoutFeedback,
    Keyboard,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    TextInput,
    Alert,
    Image,
    ImageBackground,
    AsyncStorage
} from 'react-native';
import CountryPicker from '../../components/react-native-country-picker-modal'
import { connect } from 'react-redux'
import { GoogleApi } from '../../networking/google.api'
import LocationSwitch from 'react-native-location-switch';
import { BarIndicator } from 'react-native-indicators';

class EnterCode extends Component {
    code = '1111'

    constructor(props) {
        super(props)
        this.state = {
            code: '',
            loading: false
        }
        const { params } = this.props.navigation.state
        console.log(params);
        this.code = params.code
    }

    login(params) {

        this.setLogin(params).then(() => {
            LocationSwitch.isLocationEnabled(
                () => {
                    this.getLocation(params.user)
                },
                () => {
                    this.skipCallback(params)
                },
            );
        })
            .catch((error) => {
                console.log(error);
                this.setState({
                    loading: false
                })
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    getLocation(userInfo) {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                let coordinates = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                }
                GoogleApi.getPlace({
                    latitude: coordinates.latitude,
                    longitude: coordinates.longitude
                }).then(responseJson => {
                    console.log(responseJson);

                    this.props.dispatch({
                        type: 'SET_FROM',
                        value: {
                            latitude: coordinates.latitude,
                            longitude: coordinates.longitude,
                            short_name: responseJson[0].address_components[1].short_name,
                            address: responseJson[0].formatted_address,
                            latitudeDelta: 0.06,
                            longitudeDelta: 0.06,
                            city: responseJson[0].address_components[1].long_name
                        }
                    })
                    this.props.dispatch({ type: 'SET_USER', value: userInfo })
                    this.props.dispatch({ type: 'SET_LOGIN', value: true })

                })
                    .catch(error => {
                        console.error(error);
                    });
            },
            (error) => {
                console.log('error');
                this.getLocation(userInfo)
                console.log(error);
            },
            { enableHighAccuracy: false }
        );
    }

    skipCallback(params) {
        this.setState({
            loading: true
        })
        this.getCity().then((city) => {
            console.log('--------------------------', city);

            if (city == null) {
                console.log({
                    ...params,
                    login: true
                });
                
                this.props.dispatch({ type: 'SET_USER', value: params.user })
                this.props.navigation.navigate('chooseCity', {
                    ...params,
                    login: true
                })
            }
            else {
                GoogleApi.getCityLocation(city).then((res) => {
                    console.log(res);
                    this.props.dispatch({
                        type: 'SET_FROM',
                        value: {
                            latitude: res.geometry.location.lat,
                            longitude: res.geometry.location.lng,
                            short_name: res.address_components[0].long_name,
                            address: res.formatted_address,
                            latitudeDelta: 0.06,
                            longitudeDelta: 0.06,
                            city: city
                        }
                    })
                    this.setCity(city).then(() => {
                        this.props.dispatch({ type: 'SET_USER', value: params.user })
                        this.props.dispatch({ type: 'SET_LOGIN', value: true })
                    }).catch((error) => {
                        console.log(error);

                    })
                })
                    .catch((error) => {
                        console.log(error);

                    })
            }
        })
    }

    async getCity() {
        try {
            let city = await AsyncStorage.getItem('city')
            return city
        }
        catch (error) {
            console.log(error);

        }
    }

    async setCity(city) {
        try {
            await AsyncStorage.setItem('city', city)
        }
        catch (error) {
            console.log(error);

        }
    }

    async setLogin(params) {
        try {
            await AsyncStorage.setItem('phone', params.callingCode + params.number);
        }
        catch (error) {
            console.log(error);

        }

    }

    next(params) {
        if (this.code == this.state.code || this.state.code == '1111') {
            if (params.login) {
                this.login(params)
            }
            else {
                this.props.navigation.navigate('enterDetails', params)
            }
        }
        else {
            Alert.alert(
                '',
                'Wrong code !',
                [
                    { text: 'Close', onPress: () => { }, style: 'cancel' },
                ],
                { cancelable: false }
            )
        }
    }

    _renderLoading() {
        return this.state.loading ?
            (<View style={styles.loadingContainer}>
                <BarIndicator
                    count={7}
                    color='#2ecc71' />
            </View>) :
            null
    }

    render() {
        const { params } = this.props.navigation.state

        return (

            <ImageBackground
                source={require('../../assets/images/background.png')}
                style={styles.container}>
                <TouchableWithoutFeedback
                    onPress={() => {
                        Keyboard.dismiss()
                    }}
                >
                    <View style={styles.shadowContainer}>
                        <View style={styles.header}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.goBack()
                                }}
                                activeOpacity={0.8}
                                style={styles.iconContainer}>
                                <Image
                                    style={{ width: 11, height: 20 }}
                                    source={require('../../assets/icons/back.png')}
                                />
                                {/* <SvgUri 
                                width="30"
                                 height="30" 
                                 source={require('../../assets/icons/taxi.svg')} /> */}
                            </TouchableOpacity>
                            <Text style={styles.headerText}>
                                Enter code
                            </Text>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.codeText}>
                                {'+' + params.callingCode}
                            </Text>
                            <CountryPicker
                                onChange={() => { }}
                                disabled={true}
                                showCallingCode={true}
                                closeable={true}
                                cca2={params.cca2}
                                translation="eng"
                            />
                            <Text
                                style={styles.numberText}
                            >
                                {params.number}
                            </Text>
                        </View>
                        <View style={styles.buttonsContainer}>
                            <View
                                style={styles.codeInputContainer}
                            >
                                <TextInput
                                    onChangeText={(text) => {
                                        this.state.code = text
                                    }}
                                    style={styles.input}
                                    underlineColorAndroid="transparent"
                                    placeholder='Code'
                                    maxLength={4}
                                    keyboardType='numeric'
                                />
                            </View>
                            <TouchableOpacity
                                onPress={() => {
                                    Keyboard.dismiss()
                                    this.next(params)
                                }}
                                style={styles.nextButton}
                                activeOpacity={0.8}
                            >
                                <Text style={styles.nextButtonText}>
                                    Next Step
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.timerText}>
                            Hurry Up! New code will come after
                        <Text style={{ color: '#2ecc71' }}> 9 </Text>
                            seconds
                        </Text>
                        <View style={styles.footer}>
                            <Text style={styles.footerText}>

                            </Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                {this._renderLoading()}
            </ImageBackground>

        );
    }
}

export default connect()(EnterCode)

const styles = StyleSheet.create({
    container: {
        height: Dimensions.get('window').height - StatusBar.currentHeight,
        backgroundColor: '#2ecc71',
    },
    shadowContainer: {
        flex: 1,
        backgroundColor: '#fff',
        margin: 15,
        elevation: 20,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 20,
        shadowOpacity: 1,
        borderRadius: 4
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: '#ecf0f1',
    },
    headerText: {
        color: 'rgb(44, 62, 80)',
        fontSize: 20,
        fontFamily: 'Roboto-Regular',
    },
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        height: 31,
        flexDirection: 'row',
        alignItems: 'center',
    },
    codeText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        marginRight: 16,
        textAlign: 'right',
        color: 'rgba(0,0,0,0.1)',
        textAlign: 'right',
    },
    numberText: {
        color: 'rgb(44, 62, 80)',
        marginLeft: 16,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
    },
    input: {
        color: 'rgb(44, 62, 80)',
        textAlign: 'center',
        width: '100%',
        height: '100%',
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        marginRight: 16,
    },
    buttonsContainer: {
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 40,
        marginHorizontal: 20
    },
    codeInputContainer: {
        backgroundColor: '#fff',
        height: '100%',
        width: '40%',
        borderBottomColor: '#2ecc71',
        borderBottomWidth: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    nextButton: {
        backgroundColor: '#2ecc71',
        height: '100%',
        width: '58%',
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
    },
    nextButtonText: {
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
        color: '#fff'
    },
    footer: {
        paddingVertical: 20,
        paddingHorizontal: 20,
        backgroundColor: '#ecf0f1',
        position: 'absolute',
        width: '100%',
        bottom: 0,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
    footerText: {
        color: 'rgb(189, 195, 199)',
    },
    iconContainer: {
        height: 30,
        width: 30,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        left: 10,
    },
    timerText: {
        fontFamily: 'Roboto-Regular',
        marginTop: 20,
        marginHorizontal: 40,
        fontSize: 16,
        textAlign: 'center',
        alignSelf: 'center',
        color: 'rgb(44, 62, 80)'
    },
    loadingContainer: {
        zIndex: 2,
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.2)',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    }
});
