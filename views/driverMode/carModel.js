import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    ImageBackground,
    FlatList,
    Alert
} from 'react-native';
import { BarIndicator } from 'react-native-indicators';
import API from '../../networking/api'
import { connect } from 'react-redux'

class CarModal extends Component {
    page = 1
    api = new API()
    arr = [1]
    searchValue = ''
    page = 1
    constructor(props) {
        super(props)
        this.state = {
            cars: this.defaultCars,
            listLoading: true,
            items: []
        }
    }

    componentDidMount() {
        this.getModalList()
    }

    changeInfo(id) {
        let bodyData = {
            driver_car_model: id
        }
        this.api.changeUserInfo(bodyData, this.props.user.info).then((res) => {
            console.log(res);
            if (res == 200) {
                this.getUser()
            }
            else {
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            }

        })
            .catch((error) => {
                console.log(error);

            })
    }

    getUser() {
        this.api.users('GET', {}, this.props.user.info.phone).then((res) => {
            console.log(res);
            this.props.dispatch({ type: 'SET_USER', value: res._items[0] })
            this.props.navigation.navigate('carColors')

        })
            .catch((error) => {
                console.log(error);
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    getModalList() {
        let { params } = this.props.navigation.state
        this.api.getCarModal(this.page, this.searchValue, params.brandId).then((res) => {
            console.log(res);
            this.page++
            console.log('dddddddddddddddddddddddddddddddddddddd');
            if (res._items == 0) {
                this.setState({
                    listLoading: false
                })
            }
            else if (res._links.next) {
                this.setState({
                    items: [...this.state.items, ...res._items]
                })
            }
            else {
                this.setState({
                    items: [...this.state.items, ...res._items],
                    listLoading: false
                })
            }
        })
            .catch((error) => {
                console.log(error);
                this.setState({
                    listLoading: false
                })
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    renderFooter = () => {
        if (!this.state.listLoading) return null;
        return (
            <View
                style={{
                    width: '100%',
                    alignItems: 'center',
                    marginVertical: 16
                }}
            >
                <BarIndicator
                    size={40}
                    count={7}
                    color='#2ecc71'
                />
            </View>
        );
    };

    _renderModalList() {
        return (<FlatList
            ref={ref => this.scrollView = ref}
            contentContainerStyle={{ paddingVertical: 10 }}
            showsVerticalScrollIndicator={false}
            initialNumToRender={25}
            //maxToRenderPerBatch={1}
            onEndReachedThreshold={0.1}
            keyExtractor={(item, index) => index.toString()}
            scrollToEnd={() => { console.log('end list') }}
            style={{ width: '100%' }}
            data={this.state.items}
            renderItem={({ item }) => {
                return (<TouchableOpacity
                    onPress={() => {
                        //this.changeInfo(item._id)
                        this.props.navigation.navigate('carColors', { carModalId: item._id })
                    }}
                    style={styles.carItem}
                >
                    <Image
                        style={{
                            width: 80,
                            height: 50,
                            marginLeft: 10
                        }}
                        source={item.picture ?
                            { uri: 'data:image/png;base64,' + item.picture } :
                            require('../../assets/images/carModal.png')
                        }
                    />
                    <Text
                        numberOfLines={1}
                        style={styles.itemText1}>
                        {item.title}
                    </Text>
                </TouchableOpacity>)
            }}
            ListFooterComponent={this.renderFooter}
            onEndReached={() => {
                if (this.state.listLoading) {
                    this.getModalList()
                }
            }
            }
        />)
    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress={() => {
                    Keyboard.dismiss()
                }}
            >
                <ImageBackground
                    source={require('../../assets/images/background.png')}
                    style={styles.container}>
                    <View style={styles.header}>
                        <TouchableOpacity
                            onPress={() => {
                                Keyboard.dismiss()
                                this.props.navigation.goBack()
                            }}
                            activeOpacity={0.8}
                            style={styles.iconContainer}>
                            <Image
                                style={{ width: 11, height: 20 }}
                                source={require('../../assets/icons/back.png')}
                            />
                            <Text style={styles.backText}>
                                Back
                        </Text>
                        </TouchableOpacity>
                        <Text style={styles.headerText}>
                            Select Car Model
                    </Text>
                    </View>
                    <View style={styles.searchBar}>
                        <View style={styles.searchIconContainer}>
                            <Image
                                style={{ width: 20, height: 20 }}
                                source={require('../../assets/icons/search.png')}
                            />

                        </View>
                        <TextInput
                            onChangeText={(text) => {
                                this.searchValue = text
                                this.page = 1
                                this.setState({
                                    listLoading: true,
                                    items: []
                                })
                                let { params } = this.props.navigation.state
                                this.api.getCarModal(
                                    this.page,
                                    this.searchValue,
                                    params.brandId
                                ).then((res) => {
                                    console.log(res);
                                    this.page++
                                    this.scrollView.scrollToOffset({ x: 0, y: 0, animated: false })
                                    if (res._items == 0) {
                                        this.setState({
                                            listLoading: false
                                        })
                                    }
                                    else if (res._links.next) {
                                        this.setState({
                                            items: res._items
                                        })
                                    }
                                    else {
                                        this.setState({
                                            items: res._items,
                                            listLoading: false
                                        })
                                    }
                                }).catch((error) => {
                                    console.log(error);

                                })
                            }}
                            style={styles.input}
                            underlineColorAndroid="transparent"
                            placeholder='Enter car name'
                        />
                    </View>
                    {this._renderModalList()}
                </ImageBackground>
            </TouchableWithoutFeedback>
        );
    }
}

export default connect(
    ({ user }) => ({ user })
)(CarModal)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        height: 38,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    iconContainer: {
        position: 'absolute',
        left: 18,
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'Roboto-Bold',
        color: 'rgb(44, 62, 80)',
    },
    backText: {
        marginLeft: 10,
        color: '#2ecc71',
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    },
    searchBar: {
        height: 45,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#2ecc71',
        marginHorizontal: 20
    },
    searchIconContainer: {
        position: 'absolute',
        left: 0
    },
    input: {
        fontFamily: 'Roboto-Regular',
        color: 'rgb(44, 62, 80)',
        flex: 1,
        marginLeft: 40,
        fontSize: 16,
    },
    carItem: {
        height: 80,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemText1: {
        flex: 1,
        color: 'rgb(44, 62, 80)',
        marginLeft: 20,
        fontSize: 16,
        fontFamily: 'Roboto-Regular'
    },
});
