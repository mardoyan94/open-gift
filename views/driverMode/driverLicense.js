import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    ImageBackground,
    ScrollView,
    Dimensions,
    AsyncStorage,
    DatePickerAndroid,
    Alert
} from 'react-native';
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'
import API from '../../networking/api'
import moment from 'moment';
import { NavigationActions } from 'react-navigation'

class DriverLicense extends Component {
    api = new API()

    constructor(props) {
        super(props)
        this.state = {
            userInfo: {
                driver_licence_number: '',
                driver_licence_end_date: '',
                driver_ownership_doc_number: '',
                driver_car_licence_plate_number: ''
            },
            loading: false
        }
    }

    async getDate() {
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                // Use `new Date()` for current date.
                // May 25 2020. Month 0 is January.
                date: new Date()
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                // Selected year, month (0-11), day
                let date = new Date(year, month, day)
                console.log(moment(date).format("YYYY-MM-DD")); //YYYY-MM-DD DD.MM.YYYY

                let fullDate = moment(date).format("YYYY-MM-DD")
                console.log(fullDate);
                let obj = this.state.userInfo
                obj['driver_licence_end_date'] = fullDate
                this.setState({
                    userInfo: obj
                })

            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
        }
    }

    next() {
        console.log(this.state.userInfo);

        this.setState({
            loading: true
        })
        this.api.changeUserInfo(this.state.userInfo, this.props.user.info).then((res) => {
            console.log(res);
            if (res == 200) {
                this.getUser()
            }
            else {
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            }

        })
            .catch((error) => {
                console.log(error);

            })
    }

    nextButtonActive() {
        if (this.state.userInfo.driver_licence_number.length == 0 ||
            this.state.userInfo.driver_licence_end_date == 0 ||
            this.state.userInfo.driver_ownership_doc_number == 0 ||
            this.state.userInfo.driver_car_licence_plate_number == 0) {
            return true
        }
        else {
            false
        }
    }

    _renderDateItem() {
        return (<TouchableOpacity
            onPress={() => {
                this.getDate()
            }}
            activeOpacity={0.8}
            style={styles.inputContainer}>
            <Text
                style={[styles.input,
                { marginLeft: 3 },
                this.state.userInfo.driver_licence_end_date.length == 0 ?
                    { color: 'rgb(189, 195, 199)' } : null]}
            >
                {this.state.userInfo.driver_licence_end_date.length > 0 ?
                    this.state.userInfo.driver_licence_end_date :
                    'Driver licence end date'}
            </Text>
        </TouchableOpacity>)
    }

    _renderInput(key, placeholder) {
        return (<View style={styles.inputContainer}>
            <TextInput
                onChangeText={(text) => {
                    let obj = {}
                    obj[key] = text
                    this.setState({
                        userInfo: { ...this.state.userInfo, ...obj }
                    })
                }}
                value={this.state.userInfo[key]}
                style={styles.input}
                underlineColorAndroid="transparent"
                placeholderTextColor='rgb(189, 195, 199)'
                placeholder={placeholder}
            />
        </View>)
    }

    getUser() {
        this.api.users('GET', {}, this.props.user.info.phone).then((res) => {
            console.log(res);
            this.props.dispatch({ type: 'SET_USER', value: res._items[0] })
            this.setState({
                loading: false
            })
            Alert.alert(
                '',
                'Your driver information is changed !',
                [
                    { text: 'Close', onPress: () => { }, style: 'cancel' },
                ],
                { cancelable: false }
            )
            this.props.navigation.navigate('home')

        })
            .catch((error) => {
                console.log(error);
                this.setState({
                    loading: false
                })
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    uploadImage(key) {
        const options = {
            title: 'Select Avatar',
            //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = response.uri

                // You can also display the image using data:
                //const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.api.uploadFile(key, source, this.props.user.info).then((res) => {
                    console.log(res);
                    this.getUser()
                })
                    .catch((error) => {
                        console.log(error);

                    })
            }
        });
    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress={() => {
                    Keyboard.dismiss()
                }}
            >
                <ImageBackground
                    source={require('../../assets/images/background.png')}
                    style={styles.container}>
                    <View style={styles.header}>
                        <TouchableOpacity
                            onPress={() => {
                                Keyboard.dismiss()
                                this.props.navigation.goBack()
                            }}
                            activeOpacity={0.8}
                            style={[styles.iconContainer, { left: 18, alignSelf: 'center' }]}>
                            <Image
                                style={{ width: 11, height: 20 }}
                                source={require('../../assets/icons/back.png')}
                            />
                            <Text style={styles.backText}>
                                Back
                        </Text>
                        </TouchableOpacity>
                        <Text style={styles.headerText}>
                            Driver’s License
                    </Text>
                        <TouchableOpacity
                            onPress={() => {
                                Keyboard.dismiss()
                                const resetAction = NavigationActions.reset({
                                    index: 0,
                                    actions: [
                                      NavigationActions.navigate({ routeName: 'carBrand'}),
                                    ]
                                  })
                                  this.props.navigation.dispatch(resetAction)
                                this.props.navigation.navigate('home')
                            }}
                            activeOpacity={0.8}
                            style={[styles.iconContainer, { right: 18, alignSelf: 'center' }]}>
                            <Text style={styles.backText}>
                                Done
                        </Text>
                        </TouchableOpacity>
                    </View>
                    <ScrollView>
                        <View>
                            {this._renderInput('driver_licence_number', 'Driver license number')}
                            {this._renderDateItem()}
                            {this._renderInput('driver_ownership_doc_number', 'Driver ownership doc number')}
                            {this._renderInput('driver_car_licence_plate_number', 'Driver car licence plate number')}
                            <Text style={styles.desc}>
                                Attach a photo of the front and back sides of the driver’s license
                    </Text>
                            <View style={styles.licenseRow}>
                                <View>
                                    <Image
                                        style={styles.image}
                                        source={this.props.user.info.licence_front ?
                                            { uri: 'data:image/png;base64,' + this.props.user.info.licence_front } :
                                            require('../../assets/images/front.png')}
                                    />
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.uploadImage('licence_front')
                                        }}
                                        style={styles.licenseButton}
                                        activeOpacity={0.8}
                                    >
                                        <Text
                                            style={styles.licenseButtonText}
                                        >
                                            Front
                            </Text>
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <Image
                                        style={styles.image}
                                        source={this.props.user.info.licence_back ?
                                            { uri: 'data:image/png;base64,' + this.props.user.info.licence_back } :
                                            require('../../assets/images/back.png')}
                                    />
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.uploadImage('licence_back')
                                        }}
                                        style={styles.licenseButton}
                                        activeOpacity={0.8}
                                    >
                                        <Text
                                            style={styles.licenseButtonText}
                                        >
                                            Back
                            </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <TouchableOpacity
                                disabled={this.nextButtonActive()}
                                onPress={() => {
                                    this.next()
                                    const resetAction = NavigationActions.reset({
                                        index: 0,
                                        actions: [
                                          NavigationActions.navigate({ routeName: 'carBrand'}),
                                        ]
                                      })
                                      this.props.navigation.dispatch(resetAction)
                                    this.props.navigation.navigate('home')
                                }}
                                activeOpacity={0.8}
                                style={[styles.nextButton, this.nextButtonActive() ? { opacity: 0.5 } : null]}
                            >
                                <Text style={styles.nextButtonText}>
                                    Next
                    </Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </ImageBackground>
            </TouchableWithoutFeedback>
        );
    }
}

export default connect(
    ({ user }) => ({ user })
)(DriverLicense)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        height: Dimensions.get('window').height - 24
    },
    header: {
        height: 38,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    iconContainer: {
        padding: 3,
        position: 'absolute',
        //left: 18,
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'Roboto-Bold',
        color: 'rgb(44, 62, 80)',
    },
    backText: {
        marginLeft: 10,
        color: '#2ecc71',
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    },
    inputContainer: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#2ecc71',
        marginHorizontal: 20
    },
    input: {
        height: 45,
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
        color: 'rgb(44, 62, 80)',
        flex: 1,
        fontSize: 16,
    },
    desc: {
        lineHeight: 24,
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
        color: 'rgb(44, 62, 80)',
        marginHorizontal: 20,
        marginTop: 20
    },
    nextButton: {
        marginVertical: 50,
        alignSelf: 'center',
        height: 40,
        width: 140,
        backgroundColor: '#2ecc71',
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
    },
    nextButtonText: {
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
        color: '#fff',
    },
    licenseRow: {
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    image: {
        alignSelf: 'center',
        width: 80,
        height: 80
    },
    licenseButton: {
        marginTop: 15,
        justifyContent: 'center',
        height: 30,
        width: 113,
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#2ecc71'
    },
    licenseButtonText: {
        marginLeft: 25,
        fontSize: 14,
        fontFamily: 'Roboto-Regular'
    }
});
