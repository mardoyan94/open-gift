import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    ImageBackground,
    FlatList,
    Alert
} from 'react-native';
import { BarIndicator } from 'react-native-indicators';
import API from '../../networking/api'
import { connect } from 'react-redux'

class CarColors extends Component {
    page = 1
    api = new API()
    arr = [1]
    searchValue = ''
    constructor(props) {
        super(props)
        this.state = {
            listLoading: true,
            items: [],
            loading: false
        }
    }

    componentDidMount() {
        this.getCarColors()
    }

    _renderLoading() {
        return this.state.loading ?
            (<View style={styles.loadingContainer}>
                <BarIndicator
                    count={7}
                    color='#2ecc71' />
            </View>) :
            null
    }

    changeInfo(id) {
        this.setState({
            loading: true
        })
        let { params } = this.props.navigation.state
        let bodyData = {
            driver_car_color: id,
            driver_car_model: params.carModalId,
            //is_driver: true
        }
        this.api.changeUserInfo(bodyData, this.props.user.info).then((res) => {
            console.log(res);
            if (res==200) {
                this.getUser()
            }
            else {
                this.setState({
                    loading: false
                })
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            }

        })
            .catch((error) => {
                console.log(error);
                this.setState({
                    loading: false
                })
            })
    }

    getUser() {
        this.api.users('GET', {}, this.props.user.info.phone).then((res) => {
            console.log(res);
            this.props.dispatch({ type: 'SET_USER', value: res._items[0] })
            this.setState({
                loading: false
            })
            this.props.navigation.navigate('driverLicense')

        })
            .catch((error) => {
                console.log(error);
                this.setState({
                    loading: false
                })
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    getCarColors() {
        this.api.getCarColor(this.page, this.searchValue).then((res) => {
            console.log(res);
            this.page++
            console.log('dddddddddddddddddddddddddddddddddddddd');
            if (res._items == 0) {
                this.setState({
                    listLoading: false
                })
            }
            else if (res._links.next) {
                this.setState({
                    items: [...this.state.items, ...res._items]
                })
            }
            else {
                this.setState({
                    items: [...this.state.items, ...res._items],
                    listLoading: false
                })
            }
        })
            .catch((error) => {
                console.log(error);
                this.setState({
                    listLoading: false
                })
                Alert.alert(
                    '',
                    'Server Error !',
                    [
                        { text: 'Close', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            })
    }

    renderFooter = () => {
        if (!this.state.listLoading) return null;
        return (
            <View
                style={{
                    width: '100%',
                    alignItems: 'center',
                    marginVertical: 16
                }}
            >
                <BarIndicator
                    size={40}
                    count={7}
                    color='#2ecc71'
                />
            </View>
        );
    };

    _renderCarColors() {
        return (<FlatList
            ref={ref => this.scrollView = ref}
            contentContainerStyle={{ paddingVertical: 10 }}
            showsVerticalScrollIndicator={false}
            initialNumToRender={25}
            //maxToRenderPerBatch={1}
            onEndReachedThreshold={0.1}
            keyExtractor={(item, index) => index.toString()}
            scrollToEnd={() => { console.log('end list') }}
            style={{ width: '100%' }}
            data={this.state.items}
            renderItem={({ item }) => {
                return (<TouchableOpacity
                    onPress={() => {
                        this.changeInfo(item._id)
                        //this.props.navigation.navigate('driverLicense')
                    }}
                    style={styles.carItem}
                >
                    <View style={{
                        marginLeft: 20,
                        width: 24,
                        height: 24,
                        borderRadius: 12,
                        backgroundColor: item.code
                    }} />
                    <Text style={styles.itemText1}>
                        {item.title}
                    </Text>
                </TouchableOpacity>)
            }}
            ListFooterComponent={this.renderFooter}
            onEndReached={() => {
                if (this.state.listLoading) {
                    this.getCarColors()
                }
            }
            }
        />)

    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress={() => {
                    Keyboard.dismiss()
                }}
            >
                <ImageBackground
                    source={require('../../assets/images/background.png')}
                    style={styles.container}>
                    <View style={styles.header}>
                        <TouchableOpacity
                            onPress={() => {
                                Keyboard.dismiss()
                                this.props.navigation.goBack()
                            }}
                            activeOpacity={0.8}
                            style={styles.iconContainer}>
                            <Image
                                style={{ width: 11, height: 20 }}
                                source={require('../../assets/icons/back.png')}
                            />
                            <Text style={styles.backText}>
                                Back
                        </Text>
                        </TouchableOpacity>
                        <Text style={styles.headerText}>
                            Select Car Color
                    </Text>
                    </View>
                    {this._renderCarColors()}
                    {this._renderLoading()}
                </ImageBackground>
            </TouchableWithoutFeedback>
        );
    }
}

export default connect(
    ({ user }) => ({ user })
)(CarColors)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        height: 38,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    iconContainer: {
        position: 'absolute',
        left: 18,
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'Roboto-Bold',
        color: 'rgb(44, 62, 80)',
    },
    backText: {
        marginLeft: 10,
        color: '#2ecc71',
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    },
    carItem: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',

    },
    itemText1: {
        color: 'rgb(44, 62, 80)',
        marginLeft: 20,
        fontSize: 16,
        fontFamily: 'Roboto-Regular'
    },
    loadingContainer: {
        zIndex: 2,
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.2)',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    }
});
