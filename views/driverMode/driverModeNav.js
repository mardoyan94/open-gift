import { StackNavigator } from 'react-navigation';
import CarBrand from './carBrand'
import DriverLicense from './driverLicense'
import CarColors from './carColors'
import CarModal from './carModel'

export const DriverModNav = StackNavigator({
    carBrand:{
        screen: CarBrand
    },
    carModal:{
        screen: CarModal
    },
    carColors:{
        screen: CarColors
    },
    driverLicense:{
        screen: DriverLicense
    },
},
    {
        headerMode: 'none',
        initialRouteName: 'carBrand'
    });