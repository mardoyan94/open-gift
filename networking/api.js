import Base64 from './base64'

export default class API {
    countries() {
        // try {
        //     let data = await fetch('http://ak-taxi.com:5000/countries',
        //         {
        //             method: 'GET',
        //             headers: {
        //                 Accept: 'application/json',
        //                 'Content-Type': 'application/json',
        //                 'Authorization': 'Basic ' + btoa('admin:admin'),
        //             }
        //         }
        //     )
        //     let result = await data.json()
        //     return result
        // }
        // catch (error) {
        //     console.log(error);
        // }

        return fetch('http://ak-taxi.com:5000/countries',
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                }
            }
        )
            .then((response) => {
                console.log(response);

                return response.json()
            })
            .then((responseJson) => {
                return responseJson;
            })
            .catch((error) => {
                console.log('error');

                console.error(error);
            });
    }
    //'/cities/?where='{"title":{"$regex":"Aba.*"}}'
    cities(countryId, page, searchKey) {
        let where = JSON.stringify({
            country: countryId,
            title: { "$regex": searchKey + ".*" }
        })
        return fetch(`http://ak-taxi.com:5000/cities?where=${where}&page=${page}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    users(method, bodyData, phone) {
        if (method == 'GET') {
            let where = JSON.stringify({ "phone": phone })
            let url = `http://ak-taxi.com:5000/users?where=${where}`
            console.log(url);
            return fetch(url,
                {
                    method: method,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                    },
                }
            )
                .then((data) => data.json())
                .then((result) => result)
                .catch((error) => { console.log(error); })
        }
        else if (method == 'POST') {
            return fetch('http://ak-taxi.com:5000/users',
                {
                    method: method,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                    },
                    body: JSON.stringify(bodyData)
                }
            )
                .then((data) => data.json())
                .then((result) => result)
                .catch((error) => { console.log(error); })
        }

    }
    drivings(method, bodyData, page, orderId, etag) {
        if (method == 'POST') {
            console.log('ggggggggggggggggggggggg');
            console.log(bodyData);

            return fetch(`http://ak-taxi.com:5000/drivings`,
                {
                    method: method,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                    },
                    body: JSON.stringify(bodyData)
                }
            )
                .then((data) => {
                    console.log('data+++++++++++++++++++');

                    console.log(data);

                    return data.json()
                })
                .then((result) => result)
                .catch((error) => { console.log(error); })
        }
        else if (method == 'GET' && orderId) {
            return fetch(`http://ak-taxi.com:5000/drivings/${orderId}`,
                {
                    method: method,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                    },
                }
            )
                .then((data) => data.json())
                .then((result) => result)
                .catch((error) => { console.log(error); })
        }
        else if (method == 'GET') {
            let where = JSON.stringify(bodyData)
            return fetch(`http://ak-taxi.com:5000/drivings?page=${page}&where=${where}&sort=[("_id",-1)]`,
                {
                    method: method,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                    },
                }
            )
                .then((data) => {
                     console.log('~~~~~~~~~~~~~~~~~~~~~~~');
                     console.log(data);
                    return data.json()
                })
                .then((result) => result)
                .catch((error) => { console.log(error); })
        }
        else if (method == 'PATCH') {
            console.log('patch(((((((((((((((((((((((((((((((((((((((((((((((((((((((((');

            console.log(bodyData);
            return fetch(`http://ak-taxi.com:5000/drivings/${orderId}`,
                {
                    method: method,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                        "If-Match": etag
                    },
                    body: JSON.stringify(bodyData)
                }
            )
                .then((data) => {
                    console.log('responce data');

                    console.log(data);

                    return data.json()
                })
                .then((result) => result)
                .catch((error) => { console.log(error); })
        }
        else if (method == 'DELETE') {
            return fetch(`http://ak-taxi.com:5000/drivings/${orderId}`,
                {
                    method: method,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                        "If-Match": etag
                    },
                }
            )
                .then((data) => {
                    console.log(data);
                    return data
                })
                .catch((error) => {
                    console.log(error);
                })
        }

    }

    uploadFile(key, file, user) {
        console.log('dddddddddddddddddddd');
        console.log(file);
        console.log("---------user------");
        console.log(user);


        let body = new FormData();
        body.append(key, {
            uri: file,
            type: 'image/jpeg', // or photo.type
            name: 'testPhotoName'
        });
        //body.append('Content-Type', 'image/jpg');
        return fetch(`http://ak-taxi.com:5000/users/${user._id}`,
            {
                method: 'PATCH',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                    "If-Match": user._etag
                },
                body: body
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    async getCarBrands(page, searchKey) {
        let where = JSON.stringify({
            //country: countryId,
            title: { "$regex": searchKey + ".*" }
        })
        return fetch(`http://ak-taxi.com:5000/car_brands?where=${where}&page=${page}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    async getCarModal(page, searchKey, brandId) {

        let where = JSON.stringify({
            //country: countryId,
            title: { "$regex": searchKey + ".*" },
            brand: brandId
        })
        return fetch(`http://ak-taxi.com:5000/car_models?where=${where}&page=${page}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
            }
        )
            .then((data) => {
                console.log('getCarModal');

                console.log(data);

                return data.json()
            })
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    async getCarColor(page, searchKey) {
        let where = JSON.stringify({
            //country: countryId,
            title: { "$regex": searchKey + ".*" }
        })
        return fetch(`http://ak-taxi.com:5000/car_colors?where=${where}&page=${page}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    changeUserInfo(bodyData, user) {
        return fetch(`http://ak-taxi.com:5000/users/${user._id}`,
            {
                method: 'PATCH',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                    "If-Match": user._etag
                },
                body: JSON.stringify(bodyData)
            }
        )
            .then((data) => {
                console.log(data);

                return data.status
            })
            .then((result) => result)
            .catch((error) => { console.log(error); })

    }

    createDrivingsOffer(bodyData) {
        return fetch('http://ak-taxi.com:5000/driving_offers',
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
                body: JSON.stringify(bodyData)
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }
    createDriverRating(bodyData) {
        console.log('bodyDataaaaaaaaa', bodyData);
        return fetch('http://ak-taxi.com:5000/rating',
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
                body: JSON.stringify(bodyData)
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    changeDrivingsOffer(bodyData, offerId, etag) {
        return fetch('http://ak-taxi.com:5000/driving_offers/' + offerId,
            {
                method: 'PATCH',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                    "If-Match": etag
                },
                body: JSON.stringify(bodyData)
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    getDrivingsOffers(bodyData, page) {
        let where = JSON.stringify(bodyData)

        return fetch(`http://ak-taxi.com:5000/driving_offers?page=${page}&where=${where}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
                //body: JSON.stringify(bodyData)
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    deleteOffer(offerId, etag) {
        return fetch('http://ak-taxi.com:5000/driving_offers/' + offerId,
            {
                method: 'DELETE',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                    "If-Match": etag
                },
                //body: JSON.stringify(bodyData)
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    deleteAllOffers(bodyData) {
        let where = JSON.stringify(bodyData)
        return fetch('http://ak-taxi.com:5000/driving_offers?' + where,
            {
                method: 'DELETE',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                    //"If-Match": etag
                },
                //body: JSON.stringify(bodyData)
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    getUserById(id) {
        console.log('////////////http://ak-taxi.com:5000/users/' + id);

        return fetch('http://ak-taxi.com:5000/users/' + id,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    getCarModalById(id) {
        return fetch(`http://ak-taxi.com:5000/car_models/${id}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
            }
        )
            .then((data) => {
                console.log(data);

                return data.json()
            })
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    getOffer(offerId) {
        return fetch('http://ak-taxi.com:5000/driving_offers/' + offerId,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
                //body: JSON.stringify(bodyData)
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    getColorById(id) {
        return fetch(`http://ak-taxi.com:5000/car_colors/${id}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    getPrice(distance) {
        return fetch(`http://ak-taxi.com:5000/get_price?distance=${distance}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }
    getRergistrationCode(phone) {
        return fetch(`http://ak-taxi.com:5000/send_sms?phone=${phone}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }

    getDrivingsByLocation(bodyData, page, coords, userId) {
        let where = JSON.stringify(bodyData)
        return fetch(`http://ak-taxi.com:5000/drivings?page=${page}&where=location_from_lat> ${coords.latitude - 0.2} and location_from_lat < ${coords.latitude + 0.2}  and location_from_lng > ${coords.longitude - 0.2} and location_from_lng < ${coords.longitude + 0.2} and customer!="${userId}" and status=="new"&sort=[("_id",-1)]`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + Base64.btoa('admin:admin'),
                },
            }
        )
            .then((data) => data.json())
            .then((result) => result)
            .catch((error) => { console.log(error); })
    }
}