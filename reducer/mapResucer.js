map = {
  from: {},
  to: {},
  driverLocation: {}
};

export default (state = map, action) => {
  switch (action.type) {
    case "SET_FROM":
      return { ...state, from: action.value }
    case "SET_DRIVER_LOCATION":
      return { ...state, driverLocation: action.value }
  }
  return state;
};
