config = {
    login: false,
    photoLoading: false,
    enterLocation: false
};

export default (state = config, action) => {
    switch (action.type) {
        case "SET_LOGIN":
            return { ...state, login: action.value }
            case "SET_PHOTO_LOADING":
            return { ...state, photoLoading: action.value }
            case "ENTER_LOCATION":
            return { ...state, enterLocation: action.value }
    }
    return state;
};
