user = {
    info: {},
    activeDriving: true
};

export default (state = user, action) => {
    switch (action.type) {
        case "SET_USER":
            return { ...state, info: { ...state.info, ...action.value } }
        case "SET_DRIVING":
            return { ...state, activeDriving: action.value }
        case "REMOVE_USER":
            return { ...state, info: action.value }
    }
    return state;
};
