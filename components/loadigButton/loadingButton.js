import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native'
import { BallIndicator } from 'react-native-indicators';

export default class LoadingButton extends Component {
    bttWidth = new Animated.Value(this.props.width)
    bttBorderRadius = new Animated.Value(4)
    bttLoading = false
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
        }
    }

    start() {
        this.setState({
            loading: true
        })
        Animated.parallel([
            Animated.timing(this.bttBorderRadius, {
                toValue: 20,
                duration: 200
            }),
            Animated.timing(this.bttWidth, {
                toValue: 40,
                duration: 200
            })
        ]).start(() => {
            this.props.onPressCallback()
        });
    }

    stop() {
        Animated.parallel([
            Animated.timing(this.bttBorderRadius, {
                toValue: 4,
                duration: 200
            }),
            Animated.timing(this.bttWidth, {
                toValue: this.props.width,
                duration: 200
            })
        ]).start(() => {
            this.setState({
                loading: false
            })
        });
    }


    _renderBttText() {
        return this.state.loading ? (
            <BallIndicator
                size={20}
                color='white'
            />) :
            <Text style={styles.bttText}>
                {this.props.text}
            </Text>
    }

    pressBtt() {
        this.start()

    }


    render() {
        return (
            <TouchableWithoutFeedback
                disabled={this.props.disabled}
                onPress={() => this.pressBtt()}
            >
                <View
                    style={[{
                        width: '47%',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }]}
                >
                    <Animated.View
                        style={[
                            styles.btt,
                            {
                                borderRadius: this.bttBorderRadius,
                                width: this.bttWidth
                            }
                        ]}
                    >
                        {this._renderBttText()}
                    </Animated.View>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    btt: {
        marginTop: 10,
        height: 40,
        backgroundColor: '#2ecc71',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    bttText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: '#fff',
    },
})