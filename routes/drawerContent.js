import React from 'react'
import {
  StyleSheet,
  ScrollView,
  Text,
  View,
  Image,
  TouchableNativeFeedback,
  AsyncStorage,
  TouchableOpacity
} from 'react-native';
import { DrawerItems, SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker';
import API from '../networking/api'
import { BallIndicator } from 'react-native-indicators';
import { newTripInterval, changeInterval } from '../views'

const api = new API()

function navigate(props, routeName) {
  if (routeName == 'driverModNav') {
    if (props.user.info.driver_car_model) { //
      props.navigation.navigate('settingsNav')
    }
    else {
      props.navigation.navigate(routeName)
    }
  }
  else {
    props.navigation.navigate(routeName)
  }

}

function logout() {
  return AsyncStorage.clear()
  //return AsyncStorage.clear()
}

function getUser(props) {
  api.users('GET', {}, props.user.info.phone).then((res) => {
    console.log(res);

    props.dispatch({ type: 'SET_USER', value: res._items[0] })
    props.dispatch({ type: 'SET_PHOTO_LOADING', value: false })

  })
    .catch((error) => {
      console.log(error);
      props.dispatch({ type: 'SET_PHOTO_LOADING', value: false })
      Alert.alert(
        '',
        'Server Error !',
        [
          { text: 'Close', onPress: () => { }, style: 'cancel' },
        ],
        { cancelable: false }
      )
    })
}

function uploadImage(props) {
  const options = {
    title: 'Select Avatar',
    //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      const source = response.uri

      // You can also display the image using data:
      //const source = { uri: 'data:image/jpeg;base64,' + response.data };
      props.dispatch({ type: 'SET_PHOTO_LOADING', value: true })
      api.uploadFile('avatar', source, props.user.info).then((res) => {
        console.log(res);
        getUser(props)
      })
        .catch((error) => {
          console.log(error);
          props.dispatch({ type: 'SET_PHOTO_LOADING', value: false })
        })
    }
  });
}

function _renderLoading(props) {
  return props.config.photoLoading ?
    (<View style={styles.loadingContainer}>
      <BallIndicator
        size={25}
        color='#fff'
      />
    </View>) : null
}

const DrawerContent = (props) => { 
  return (
    <ScrollView style={{ backgroundColor: '#27ae60' }}>
      <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
        <View style={styles.header}>
          <View>
            {_renderLoading(props)}
            <TouchableOpacity
              onPress={() => {
                uploadImage(props)
              }}
              activeOpacity={0.8}
              style={styles.photoContainer}>
              <Image
                style={styles.image}
                source={props.user.info.avatar ?
                  { uri: 'data:image/png;base64,' + props.user.info.avatar } :
                  require('../assets/images/1.png')
                }
              />
            </TouchableOpacity>
          </View>
          <View>
            <Text style={styles.infoText}>
              {props.user.info.firstname}{'\n'}
              +{props.user.info.phone}{'\n'}
              {props.user.info.lastname}
            </Text>
          </View>
        </View>
        <TouchableNativeFeedback
          onPress={() => {
            navigate(props, 'requestHistory')
          }}
        >
          <View style={styles.item}>
            <Image
              style={[styles.icon, { 
                width: 25.5
              }]}
              source={require('../assets/icons/history.png')}
            />
            <Text style={styles.itemText}>
              Request History
            </Text>
          </View>
        </TouchableNativeFeedback>
        {props.user.info.is_driver ?
          <TouchableNativeFeedback
            onPress={() => {
              navigate(props, 'driverHistory')
            }}
          >
            <View style={styles.item}>
              <Image
                style={styles.icon}
                source={require('../assets/icons/route.png')}
              />
              <Text style={styles.itemText}>
                Driver Rides
              </Text>
            </View>
          </TouchableNativeFeedback> : null}
        {props.user.info.is_driver ?
          <TouchableNativeFeedback
            onPress={() => {
              navigate(props, 'nearbyDrivingsNav')
            }}
          >
            <View style={styles.item}>
              <Image
                style={[styles.icon, { width: 17.5 }]}
                source={require('../assets/icons/placeholder.png')}
              />
              <Text style={styles.itemText}>
                Nearby Drivings
              </Text>
            </View>
          </TouchableNativeFeedback> : null}
        <TouchableNativeFeedback
          onPress={() => {
            navigate(props, 'settingsNav')
          }}
        >
          <View style={styles.item}>
            <Image
              style={styles.icon}
              source={require('../assets/icons/settings.png')}
            />
            <Text style={styles.itemText}>
              Settings
            </Text>
          </View>
        </TouchableNativeFeedback>
        <TouchableNativeFeedback
          onPress={() => {
            navigate(props, 'driverModNav')
          }}
        >
          <View style={styles.item}>
            <Image
              style={styles.icon}
              source={require('../assets/icons/taxi2.png')}
            />
            <Text style={styles.itemText}>
              Driver mode
            </Text>
          </View>
        </TouchableNativeFeedback>
        <TouchableNativeFeedback
          onPress={() => {
            logout().then(() => {
              console.log(newTripInterval);
              clearInterval(newTripInterval)
              changeInterval()
              props.dispatch({ type: 'REMOVE_USER', value: {} })
              props.dispatch({ type: 'SET_LOGIN', value: false })
            })
              .catch((error) => {
                console.log(error)
              })
          }}
        >
          <View style={styles.item}>
            <Image
              style={styles.icon}
              source={require('../assets/icons/logout.png')}
            />
            <Text style={styles.itemText}>
              Log Out
          </Text>
          </View>
        </TouchableNativeFeedback>
      </SafeAreaView>
    </ScrollView>
  );
}

export default connect(
  ({ user, config }) => ({ user, config })
)(DrawerContent)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
    borderBottomWidth: 1,
    borderBottomColor: '#2ecc71'
  },
  icon: {
    width: 24,
    height: 24,
    marginLeft: 20
  },
  itemText: {
    marginLeft: 15,
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    color: '#fff'
  },
  header: {
    height: 90,
    backgroundColor: '#2ecc71',
    alignItems: 'center',
    flexDirection: 'row',
  },
  photoContainer: {
    marginLeft: 15,
    height: 55,
    width: 55,
    borderRadius: 28,
  },
  image: {
    flex: 1,
    width: '100%',
    borderRadius: 28
  },
  infoText: {
    marginLeft: 11,
    fontFamily: 'Roboto-Regular',
    fontSize: 12,
    color: '#fff',
    lineHeight: 16
  },
  loadingContainer: {
    zIndex: 2,
    position: 'absolute',
    height: 55,
    width: 55,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  }
});