import { DrawerNavigator } from "react-navigation";
import {
    Home,
    HistoryNavigator,
    DriverHistoryNavigator,
    NearbyDrivingsNav,
    DriverModNav,
    SettingsNav,
    ClientOrderMap,
    OrderMap,
    
} from '../views'
import DrawerContent from './drawerContent'

const Drawer = DrawerNavigator(
    {
        home: {
            screen: Home
        },
        requestHistory: {
            screen: HistoryNavigator
        },
        driverHistory: {
            screen: DriverHistoryNavigator
        },
        nearbyDrivingsNav: {
            screen: NearbyDrivingsNav
        },
        orderMap: {
            screen: OrderMap
        },
        driverModNav: {
            screen: DriverModNav
        },
        settingsNav: {
            screen: SettingsNav
        },
        clientOrderMap: {
            screen: ClientOrderMap
        }
    },

    {
        navigationOptions: {
            drawerLockMode: 'locked-closed'
          },
        contentComponent: DrawerContent,
        //initialRouteName: 'requestHistory'
    }
);

export default Drawer;
